/**
 *     This file is part of SPARK-OMOP.
 *
 *     SPARK-OMOP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     SPARK-OMOP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 *     This file is part of OMOP-SPARK.
 *
 *     OMOP-SPARK is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OMOP-SPARK is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.interchu.omop

import io.frama.interchu.omop._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Column
import io.frama.interchu.omop.ConfigYaml._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
import java.nio.file.Paths
import io.frama.parisni.spark.csv.CSVTool
import io.frama.parisni.spark.dataframe.DFTool

object Dispatcher {

  /**
   * PRINCIPLE:
   *
   * The dispatching step write partitionned parquet files and change the mapDf
   * content to filter based on those intermediate files.
   *
   * The benfit from using partitionned files is the following step will only read the partition
   * eg: condition will be partitionned into two: condition and procedure.
   *
   * The following procedure step will read the condition.procedure partition and transform it
   * into real procedure.
   *
   * One advantage of this mecanism is it allows multiple behavior of dispatching and also cache
   * will be optinnal.
   *
   *  TODO:
   *  - if intermediate partition is allowed, write to disk and
   *  - create the transformation A->B where A and B are omop structure supposed to be dispatched togother
   */

  def dispatchAll(spark: SparkSession, conf: Config, dfMap: Map[String, DataFrame], structs: Map[String, StructType]): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]() ++ dfMap

    // for each dispatch ``from`` write partitions
    conf.dispatch.get.foreach(f => {

      // only if ``from`` is active
      if (conf.resources.filter(z => z.name == f.from)(0).active) {

        val partDf = addDomain(dfMap.get(f.from).get, dfMap.get("concept_pivot").get, f, conf.tmpFolder)
        result(f.from) = partDf

      }
    })

    // for each dispatch ``from``
    conf.dispatch.get.foreach(f => {

      // only if ``from`` is active
      if (conf.resources.filter(z => z.name == f.from)(0).active) {

        // for each dispatch ``to``
        f.to.foreach(x => {

          // add the transformed ``from``
          result(x.name) = result.get(x.name).get
            .union(DFTool.applySchemaSoft(
              transform(from = result.get(f.from).get, dispatch = x), structs.get(x.name).get))

        })
      }
    })

    dfMap ++ result.toMap
  }

  /**
   * Add a domain column and partition on that column
   * Returns the partitionned dataframe
   */
  def addDomain(from: DataFrame, concept_pivot: DataFrame, dispatch: DispatchFrom, tempFolder: String): DataFrame = {
    val file = java.nio.file.Paths.get(tempFolder, dispatch.localDomain).toString()
    from
      .join(concept_pivot, from(dispatch.columnDispatch) === concept_pivot("concept_id"), "left")
      .drop("concept_id")
      .withColumn("dispatcher_domain", coalesce(col("standard_domain_id"), lit(dispatch.localDomain)))
      .drop("standard_domain_id","concept_id","standard_concept_id")
      .write
      .mode(org.apache.spark.sql.SaveMode.Overwrite)
      .format("parquet")
      .partitionBy("dispatcher_domain")
      .save(file)

    from.sparkSession.read.format("parquet").load(file)
  }

  /**
   * Takes a partitionned dataframe and apply the
   * Dispatch reciepy
   */
  def transform(from: DataFrame, dispatch: DispatchTo): DataFrame = {
    var tmp = from
      .where(lower(col("dispatcher_domain")).equalTo(lower(lit(dispatch.standardDomain))))

    val cols = dispatch.dispatchFields.filter(f => f.from.isDefined).map(x => {
      tmp = tmp.withColumnRenamed(x.from.get, x.name)
      col(x.name)
    })
    tmp.select(cols: _*)
  }

}

