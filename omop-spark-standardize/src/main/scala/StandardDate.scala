/**
 *     This file is part of SPARK-OMOP.
 *
 *     SPARK-OMOP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     SPARK-OMOP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 *     This file is part of OMOP-SPARK.
 *
 *     OMOP-SPARK is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OMOP-SPARK is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.interchu.omop

import io.frama.interchu.omop._
import io.frama.interchu.omop.ConfigYaml._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Column
import org.apache.spark.sql.types._
import com.typesafe.scalalogging.LazyLogging
import io.frama.parisni.spark.dataframe.DFTool

object StandardDate extends LazyLogging {

  /**
   * Make a lookup for each standard concept id
   * to the concept table
   *
   */

  def dateAll(dfMap: Map[String, DataFrame], conf: Config): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]()

    dfMap.filter(f => f._1 != "concept_pivot")
      .foreach(f => {
        result(f._1) = date(
          f._2,
          conf.resources.filter(d => d.name == f._1)(0).schema.fields)
      })

    dfMap ++ result.toMap
  }

  def date(df: DataFrame, fields: List[Fields]): DataFrame = {
    var tmp = df
    for (f <- fields) {
      if (f.name.matches(".*_date$")) {
        val dateTimeColumn = getDatetime(f.name)
        if (!fields.filter(p => p.name == dateTimeColumn).isEmpty)
          tmp = populateDate(tmp, f.name, dateTimeColumn)
      }
    }
    tmp
  }

  def populateDate(df: DataFrame, dateColumn: String, dateTimeColumn: String): DataFrame = {
    df.withColumn(dateColumn, df.col(dateTimeColumn).cast(DateType))

  }

  def getDatetime(c: String): String = {
    val pat = "^(.*?)_date$".r
    val pat(tmp) = c
    tmp + "_datetime"
  }
}
