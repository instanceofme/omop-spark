/**
 *     This file is part of SPARK-OMOP.
 *
 *     SPARK-OMOP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     SPARK-OMOP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 *     This file is part of OMOP-SPARK.
 *
 *     OMOP-SPARK is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OMOP-SPARK is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.interchu.omop

import io.frama.interchu.omop.ConfigYaml._

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
import java.nio.file.Paths
import io.frama.parisni.spark.csv.CSVTool
import io.frama.parisni.spark.dataframe.DFTool

object Initializer {

  /**
   *  Create a map of empty omop dataframe
   *  with the schma specified in the config
   */

  def init(spark: SparkSession, structs: Map[String, StructType]): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]()

    structs.foreach(f => {
      result(f._1) = DFTool.createEmptyDataFrame(spark, f._2)
    })
    result.toMap
  }
}