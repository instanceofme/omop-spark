/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.interchu.omop

import com.amazon.deequ.VerificationSuite
import com.amazon.deequ.checks.{Check, CheckLevel, CheckStatus}
import com.amazon.deequ.constraints.ConstraintStatus
import com.typesafe.scalalogging.LazyLogging
import io.frama.interchu.omop.ConfigYaml._
import org.apache.spark.sql.DataFrame

import scala.collection.mutable.ArrayBuffer

object QualityOutput extends LazyLogging {

  /**
   * Validate the Schema
   * and produce a map of DataFrame
   * Also:
   *  - null columns
   *  - cast columns
   *
   */

  def checkAll(dfMap: Map[String, DataFrame], conf: Config): Map[String, DataFrame] = {

    dfMap.foreach(f => {
      if (conf.resources.filter(t => t.name == f._1)(0).active) {
        val checks = generateConstraintsChecks(conf.resources.filter(t => t.name == f._1)(0).schema.fields)
        validateChecks(f._1, f._2, checks)
      }
    })

    dfMap
  }

  //  def generateForeignChecks(fields: List[ForeignKeys]): Array[Check] = {
  //
  //  }

  def generateConstraintsChecks(fields: List[Fields]): Array[Check] = {
    var result = ArrayBuffer[Check]()

    fields
      .filter(f => f.constraints.isDefined)
      .foreach(f => {
        if (f.constraints.get.required.getOrElse(false))
          result += Check(CheckLevel.Error, "Required constraint").isComplete(f.name)
        if (f.constraints.get.unique.getOrElse(false))
          result += Check(CheckLevel.Error, "Unique constraint").isUnique(f.name)
        if (f.constraints.get.minimum.isDefined)
          result += Check(CheckLevel.Error, "Minimum constraint").hasMin(f.name, _ >= f.constraints.get.minimum.get)
        if (f.constraints.get.maximum.isDefined)
          result += Check(CheckLevel.Error, "Maximum constraint").hasMax(f.name, _ <= f.constraints.get.maximum.get)
        if (f.constraints.get.pattern.isDefined)
          result += Check(CheckLevel.Error, "Pattern constraint").hasPattern(f.name, f.constraints.get.pattern.get.r)
        if (f.constraints.get.enum.isDefined)
          result += Check(CheckLevel.Error, "Enum constraint").isContainedIn(f.name, f.constraints.get.enum.get)
      })

    result.toArray
  }

  def validateChecks(dataName: String, data: DataFrame, checks: Array[Check]): Unit = {
    val verificationResult = VerificationSuite()
      .onData(data)
      .addChecks(checks)
      .run()

    if (verificationResult.status == CheckStatus.Success) {
      logger.warn("The **%s** table passed the test, everything is fine!".format(dataName))
    } else {
      logger.error("We found errors in the  **%s** table, the following constraints were not satisfied:\n"
        .format(dataName))

      val resultsForAllConstraints = verificationResult.checkResults
        .flatMap { case (_, checkResult) => checkResult.constraintResults }

      resultsForAllConstraints
        .filter {
          _.status != ConstraintStatus.Success
        }
        .foreach { result =>
          logger.error(s"${result.constraint} failed: ${result.message.get}")
        }
    }

  }

}

