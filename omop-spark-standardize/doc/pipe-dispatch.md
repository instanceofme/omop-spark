The `dispatching` pipe redirects rows to the standard domain the local code has
beenmapped to. The dispatching recipy is described in the yaml config file.

The `primary key` of the resulting dispatched tables are populated with null
values.
