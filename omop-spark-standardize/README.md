# OMOP Data Standardization

This Data Standardization pipeline **ease** and **improve** the production of dataset in the OMOP format. 
It takes local informations as OMOP tables input and produces OMOP standardized
tables as output.

There is multiple available pipe for the pipeline:

- [x] [reader](omop-spark-standardize/doc/pipe-reader.md)
- [x] [lookup for local concept_id](omop-spark-standardize/doc/pipe-local-concept.md)
- [x] [lookup for standard concept_id](omop-spark-standardize/doc/pipe-standard-concept.md)
- [x] [date extraction](omop-spark-standardize/doc/pipe-date.md)
- [x] [dispatching to other domain](omop-spark-standardize/doc/pipe-dispatch.md)
- [ ] [unit translation](omop-spark-standardize/doc/pipe-unit-translation.md)
- [ ] [optimized calculated tables](omop-spark-standardize/doc/pipe-calculated-tables.md)
- [x] [quality of input tables](omop-spark-standardize/doc/pipe-quality-input.md)
- [x] [quality of output tables](omop-spark-standardize/doc/pipe-quality-output.md)
- [x] [writer](omop-spark-standardize/doc/pipe-writer.md)

# Principle

The pipeline is written in **spark scala**. Scala is preferred here over python
because it offers more library than python. One drawback of the choice is to
limit the potential developper for this pipeline. An advantage is it has only
java as dependency.

The pipeline reads specifications from a unique **yaml config file**. An
example of the file can be [found here](omop-spark-standardize/test/config.yaml). The omop table specification is
inspired from **table schema** and the config file can be easily transformed
into this standard specification with this [python library](https://framagit.org/interchu/table-schema-translator). All the omop
schema can be modified to adapt to some schema evolution or local specificity.

The source files are read from an **inputFolder**, eventually it writes
intermediate files into a **tempFolder** and finally produces results in the
**outputFolder**. The files format can be *csv*, *parquet* or *orc* files;
**parquet** is the prefered format for performances. The datasets can be also
retrived from *hive* tables.

![pipeline](omop-spark-standardize/doc/diag.png)

# Important notes on input files

1. The source files column order has no importance
1. Any column referenced in the yaml config file will be kept/added
1. Any column not referenced in the yaml config file will be removed
1. The source files shall be present accordingly to: `inputFolder/<tableName>/<file.format>`
1. The `resources.active` field SHALL be to *false* if the source file is not present yet
1. In case of *hive* format, the inputFolder SHALL be the *hive database* and the resources.name be the *hive table*

# Important notes on outpout files

1. If csv is choosen as outputFormat then multiple csv will be produced. They can be merged thought unix command: `cat *.csv > merged.csv`
1. The resulting column order is the same as defined in the config file


# Example

## STEP: READ

### concept_pivot

| concept_id | concept_code | vocabulary_id | domain_id | standard_concept_id | standard_vocabulary_id | standard_domain_id |
|------------|--------------|---------------|-----------|---------------------|------------------------|--------------------|
| 1          | m            | Gender        | Person    | 202020202011        | m                      | OMOP Generated     |
| 2          | f            | Gender        | Person    | 210101010101        | f                      | OMOP Generated     |

### person

| person_id | gender_source_value |
|-----------|---------------------|
| 1         | m                   |
| 2         | f                   |

## STEP: LOCAL CONCEPT ID


| person_id | gender_source_concept_id | gender_source_value |
|-----------|--------------------------|---------------------|
| 1         | 202020202011             | m                   |
| 2         | 210101010101             | f                   |


## STEP: STANDARD CONCEPT ID


| person_id | gender_concept_id | gender_source_concept_id | gender_source_value |
|-----------|-------------------|--------------------------|---------------------|
| 1         | 2                 | 202020202011             | m                   |
| 2         | 3                 | 210101010101             | f                   |


## STEP: WRITE


| person_id | death_datetime | birth_datetime | gender_concept_id | gender_source_concept_id | gender_source_value |
|-----------|----------------|----------------|-------------------|--------------------------|---------------------|
| 1         |                |                | 2                 | 202020202011             | m                   |
| 2         |                |                | 3                 | 210101010101             | f                   |



# Installation

Download:

- the latest [spark binary](https://www.apache.org/dyn/closer.lua/spark/spark-2.4.3/spark-2.4.3-bin-hadoop2.7.tgz) and untar it somewhere (eg: `/opt/spark/current` ). The **SPARK_HOME** environment variable SHALL be specified and java installed.
- install `maven`
- [clone this repository](https://framagit.org/parisni/spark-etl) and compile `mvn install`
- [clone this repository](https://framagit.org/interchu/omop-spark) move to `omop-spark-standardize` folder and compile `mvn install`
- you should get shaded jar in the `target` folder

# Configuration



# Usage

- edit the yaml config file
- put the source data into the `inputFolder` (one folder per table)
- adapt and run the below command (this has been configured for a single server with 8core and 32G ram)


``` bash
export SPARK_HOME=/opt/spark/current

$SPARK_HOME/bin/spark-submit\
--master local[20]\ # 2x the number of cpu core
--driver-memory=20G\ # 2/3 of the total ram
--executor-memory=8G\ # 1/3 of the total ram
--class io.frama.interchu.omop.Run\
--conf spark.sql.shuffle.partitions=500\
--conf spark.default.parallelism=500\
--conf spark.task.maxFailures=20\
--conf spark.driver.maxResultSize=8G\ # 1/3 of the total ram
--conf spark.sql.broadcastTimeout=1200\
--conf spark.locality.wait=5s\
--conf spark.sql.parquet.compression.codec='uncompressed'\
--conf spark.driver.extraJavaOptions='-Djava.io.tmpdir=/tmp/driverLogs'\
--conf spark.executor.extraJavaOptions='-Djava.io.tmpdir=/tmp/executorLogs'\
--conf spark.eventlog.enabled=false\
--conf spark.eventLog.dir='/tmp/sparkLogs/'\
omop-spark-standardize-0.0.1-SNAPSHOT-shaded.jar config.yaml        
```


# Performance note

1. for some large table, you should specify the yaml config **partition** to
   some large number (eg: 1000). This will split the tasks into smaller chuncks
   and allow to process large dataset on small servers
1. distribute the pipeline on a hadoop cluster with yarn if possible

