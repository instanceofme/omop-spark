/**
 *     This file is part of SPARK-OMOP.
 *
 *     SPARK-OMOP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     SPARK-OMOP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.omop.deid

import org.apache.spark.sql.QueryTest

class ExportTest extends QueryTest with SparkSessionTestWrapper   {

  test("test export brat") {

    val df = spark.sql("""
       select 
          cast(1 as long) note_id
        , 'PRENOM' note_nlp_source_value
        , '\nnicolas \n paris' snippet
        , 200 offset_begin
        , '\nnicolas \n paris' note_text
        union all
           select 
          cast(2 as long)  note_id
        , 'PRENOM' note_nlp_source_value
        , 'bob \n le\n' snippet
        , 100 offset_begin
        , 'bob \n le\n' note_text
      """)
    val result = Export.toBrat(df)
    Export.toLocal(result, "/tmp")
  }
}


