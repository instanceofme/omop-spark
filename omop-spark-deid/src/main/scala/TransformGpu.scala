/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.omop.deid

import org.apache.spark.sql.{SparkSession, DataFrame}
import io.frama.parisni.spark.dataframe.DFTool

/**
 * This gets the result from the neural network
 * as a <result.csv.bz2> archive and produce a
 * temporary table to store the results.
 *
 * The table is joined to the <inputNoteDeid> table
 * to get some extra information for future processing.
 *
*/
object TransformGpu extends DeidTables with DeidIO with App {

  val mode = args(0)
  val database = args(1)
  val inputNoteDeid = args(2)

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Transform GPU")
    .enableHiveSupport()
    .getOrCreate()

  process(spark, mode, database, inputNoteDeid)

  /**
   * Get the result of the GPU processing and load them into hive
   *
   * @param spark   String Name of the hive database
   * @param mode   String Name of the hive database
   * @param database String Name of the note subset to process
   * @param inputNoteDeid String Name of the note subset to process
   *
   * @note MISC INPUT: compressed output of GPU (result.csv.bz2)
   * @note TABLE OUTPUT:  [[DEID_GPU_TABLE]]
   *
   */
  def process(spark: SparkSession,
              mode: String,
              database: String,
              inputNoteDeid: String) {

    readDataframe(spark, mode, database, inputNoteDeid)
      .createOrReplaceTempView("note")

    val gpu_result = spark.read.format("csv")
      .option("delimiter", ";")
      .load("result.csv.bz2")
      .withColumnRenamed("_c0", "note_id")
      .withColumnRenamed("_c1", "json")


    gpu_result.createOrReplaceTempView("gpu_result")

    val dfText = spark.sql(
      f"""
      select 
        n.note_id
      , n.person_id
      , n.note_text
      from note n
      join gpu_result g on (g.note_id = n.note_id)
      """)

    val dfJson = jsonExplode(gpu_result)
    val result = getSnippet(dfJson, dfText)

    writeTempDataframe(result, DEID_GPU_TABLE)

  }

  def jsonExplode(df: DataFrame): DataFrame = {
    df.createOrReplaceTempView("df")

    df.sparkSession.sql(
      """
      with t as (
      select note_id, explode(from_json(json,'array<struct<begin:integer,end:integer,type:string>>')) as exploded
      from df)
      select note_id, exploded.begin as offset_begin, exploded.end as offset_end, exploded.type as note_nlp_source_value
      from t
      """)
  }

  def getSnippet(df: DataFrame, text: DataFrame): DataFrame = {
    df.createOrReplaceTempView("df")
    text.createTempView("text")

    val dfSbstring = df.sparkSession.sql(
      """
      select 
        note_id
      , person_id
      , offset_begin
      , offset_end
      , substring(note_text, offset_begin + 1, offset_end - offset_begin + 1) as snippet
      , note_nlp_source_value
      , cast(null as string) term_temporal
      , 'neuroner' nlp_system
      from df
      join text using (note_id)
      """)

    DFTool.applySchema(dfSbstring, schemaNoteNlp)
  }

}
