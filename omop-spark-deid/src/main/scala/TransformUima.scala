/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.omop.deid

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
 * This takes an <inputNote> table and apply
 * a UIMA pipeline on every texts.
 * The information from observation are provided
 * from the temporary **knowldege** table.
 *
 * In order to scale, the <partitionNum> should
 * be set to a value according to the number of
 * note to process. (500 <--> 1M notes)
 *
 * The spark configuration SHALL not use multicore
 * executor since UIMA is not thread safe. Instead,
 * a large number of executor with 1 core should be
 * configured.
 */
object TransformUima extends DeidTables with DeidIO with App {

  // transient and lazy to be initialized at executor level for each partition
  //@transient lazy val deidPojo = new fr.aphp.wind.uima.deid.pojo.DeidPojoProd();

  case class Text(note_id: Long, person_id: Long, note_text: String, json: String)

  val mode = args(0)
  val database = args(1)
  val inputNote = args(2)
  val partitionNum = args(3).toInt

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Transform UIMA")
    .enableHiveSupport()
    .getOrCreate()

  process(spark, mode, database, inputNote, partitionNum)

  /**
   * Apply the UIMA DEID pipeline on data previsously populated by
   * the Extract Step
   *
   * @param spark        SparkSession spark session
   * @param mode         String csv, hive, or delta
   * @param database     String Name of the hive database
   * @param inputNote    String Name of the input note table
   * @param partitionNum Integer Number of partitions to process
   * @note TABLE OUTPUT:  [[DEID_UIMA_TABLE]]
   *
   */
  def process(spark: SparkSession,
              mode: String,
              database: String,
              inputNote: String,
              partitionNum: Int
             ) = {

    readDataframe(spark, mode, database, inputNote)
      .createOrReplaceTempView("note")

    readTempDataframe(spark, DEID_KNOWLEDGE_TABLE)
      .createOrReplaceTempView("deid_knowledge")

    val query =
      """
      select 
          n.note_id
        , n.person_id
        , n.note_text
        , j.json
        , n.note_hash
      from note n
      join deid_knowledge j
        on (n.person_id = j.person_id)
      where note_text is not null
      """

    import spark.implicits._

    val ext = spark
      .sql(query)
      .as[Text]
      .repartition(partitionNum)

    import scala.collection.JavaConverters._

    val res = ext.mapPartitions({
      partition => {
        val dp = new fr.aphp.wind.uima.deid.pojo.DeidPojoProd()
        partition.map(x => dp.analyzeText(x.note_id, x.person_id, x.note_text, x.json).asScala)
      }
    })
      .withColumn("ar", explode(col("value")))
      .selectExpr("ar._1 as note_id"
        , "ar._2 as person_id"
        , "ar._3 as offset_begin"
        , "ar._4 as offset_end"
        , "ar._5 as note_nlp_source_value"
        , "ar._6 as snippet"
        , "ar._7 as lexical_variant"
        , "ar._8 as term_temporal"
        , "ar._9 as nlp_system")
      .withColumn("term_temporal",
        when(col("term_temporal").isNotNull, concat(lit("concept_date="), col("term_temporal")))
          .otherwise(lit(null)))

    writeTempDataframe(res, DEID_UIMA_TABLE)

  }

  // val res = ext.map(x =>
  //   deidPojo.analyzeText(x.note_id, x.person_id, x.note_text, x.json)
  //     .asScala)
  //   .withColumn("ar", explode(col("value")))
  //   .selectExpr("ar._1 as note_id"
  //     , "ar._2 as person_id"
  //     , "ar._3 as offset_begin"
  //     , "ar._4 as offset_end"
  //     , "ar._5 as note_nlp_source_value"
  //     , "ar._6 as snippet"
  //     , "ar._7 as lexical_variant"
  //     , "ar._8 as term_temporal"
  //     , "ar._9 as nlp_system")
  //   .withColumn("term_temporal", when(col("term_temporal").isNotNull, concat(lit("concept_date="), col("term_temporal"))).otherwise(lit(null)))

  // writeTempDataframe(res, DEID_UIMA_TABLE)

  // the mapPartition version si two time slower

  //  def test = (partition: Iterator[Text]) => {
  //    val dp = new fr.aphp.wind.uima.deid.pojo.DeidPojoProd()
  //    val wList = new java.util.ArrayList[scala.collection.mutable.Buffer[(java.lang.Long, java.lang.Long, Integer, Integer, String, String, String, String, String)]]()
  //    println("partition size %s".format(partition.size))
  //    while (partition.hasNext) {
  //      val x = partition.next()
  //      val res: mutable.Buffer[(java.lang.Long, java.lang.Long, Integer, Integer, String, String, String, String, String)] = dp.analyzeText(x.note_id, x.person_id, x.note_text, x.json).asScala
  //      wList.add(res)
  //    }
  //    wList.iterator().asScala
  //  }
  //val res = ext.mapPartitions(p => test(p))

  // ext.map(x => deidPojo.analyzeTextCsv(x.note_id, x.person_id, x.note_text, x.json).asInstanceOf[String])
  //   .write
  //   .mode(SaveMode.Overwrite)
  //   .text(outputPath)

  // val deidDF = spark.read.option("header", false)
  // .option("quote", "\"").option("mode", "FAILFAST")
  // .option("escape", "\"").option("multiline", true)
  // .option("delimiter", ";").schema(schemaNoteNlp).csv(outputPath)

  // deidDF
  //   .withColumn("term_temporal", concat(lit("concept_date="), col("term_temporal")))
  //   .write.format("orc").mode(Overwrite).saveAsTable(DEID_UIMA_TABLE)

  //  def analyseText(ext: DataSet[Text]): DataFrame = {
  //    exp.map(x => deidPojo.analyzeText(x.note_id, x.person_id, x.note_text, x.json).asInstanceOf[String])
  //      .write
  //      .mode(SaveMode.Overwrite)
  //      .text(outputPath)
  //  }

  //  def analyseTextSpark(ext:Text) : DataFrame = {
  //    exp.map(x => deidPojo.analyzeTextSpark(x.note_id, x.person_id, x.note_text, x.json).toList)
  //
  //    spark.read.option("header", false)
  //      .option("quote", "\"").option("mode", "FAILFAST")
  //      .option("escape", "\"").option("multiline", true)
  //      .option("delimiter", ";").schema(schemaNoteNlp).csv(outputPath)
  //  }
}

// scalastyle:on println
