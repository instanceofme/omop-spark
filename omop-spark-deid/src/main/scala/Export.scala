/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.omop.deid

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import com.typesafe.scalalogging.LazyLogging

/**
 * This produce brat files into a <bratPath>
 * given some <inputNote> and <inputNoteNlp>
 * tables.
 *
 * @note the bratPath should be empty
 *
*/
object Export extends LazyLogging with DeidIO with DeidTables with App {

  val mode = args(0)
  val database = args(1)
  val inputNote = args(2)
  val inputNoteNlp = args(3)
  val bratPath = args(4)

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Export")
    .enableHiveSupport()
    .getOrCreate()

  process(spark, mode, database, inputNote, inputNoteNlp, bratPath)

  /**
   * Export a subset of note_nlp table to brat files into
   * a specified path
   *
   * @param spark String Name of the hive database
   * @param mode String Name of the hive database
   * @param database String Name of the hive database
   * @param inputNote String Name of the hive database
   * @param inputNoteNlp String Name of the hive database
   * @param bratPath Subset String The local path to write into
   *
   * @note TABLE INPUT: [[NOTE]], [[NOTE_NLP]]
   * @note TABLE OUTPUT:  texts to brat format into the bratPath folder
   *
   */
  def process(spark: SparkSession,
              mode: String,
              database: String,
              inputNote: String,
              inputNoteNlp: String,
              bratPath: String
             ) = {

    readDataframe(spark, mode, database, inputNoteNlp)
      .createOrReplaceTempView("note_nlp_tmp")

    readDataframe(spark, mode, database, inputNote)
      .createOrReplaceTempView("note_tmp")

    val noteNlpToExport = spark.sql("""
      select 
       n.note_id
      ,case 
          when nlp_system rlike 'uima' 
            then l.note_nlp_source_value
          else concat(l.note_nlp_source_value,'')
       end note_nlp_source_value
      ,n.note_text
      ,l.snippet
      ,l.offset_begin
    	,l.offset_end
      from note_nlp_tmp l
      join note_tmp n on (l.note_id = n.note_id)
      where n.note_text is not null
      """)
    logger.info("Got nlp data to export")

    val bratDf = toBrat(noteNlpToExport)
    toLocal(bratDf, bratPath)

  }

  def toBrat(df: DataFrame): DataFrame = {
    df.sparkSession.udf.register("split_map", split_map(_: String))
    df.createOrReplaceTempView("df")
    df.sparkSession.sql(
      """
      with t as (
      select 
          note_id
        , note_nlp_source_value
        , note_text
        , snippet
        , offset_begin
        , explode(split_map(snippet)) e
      from df
      )
      select 
          note_id
        , row_number() over(partition by note_id order by offset_begin asc)  row_num
        , note_nlp_source_value
        , note_text
        , offset_begin + e._1 as offset_begin 
        , offset_begin + e._2 as offset_end
        , substring(snippet, e._1 + 1 , e._2 - e._1) as snippet
      from t
      """).createOrReplaceTempView("gr")

    val result = df.sparkSession.sql(
      """
        select
        note_id
        , concat_ws('\n',collect_list(concat(
        'T', row_num
        , '\t'
        , lower(note_nlp_source_value)
        , ' '
        , offset_begin
        , ' '
        , offset_end
        , '\t'
        , snippet))) row
        , note_text
        from gr
        group by note_id, note_text
        """)
    logger.info("Formated data")

    result
  }

  def toLocal(df: DataFrame, folder: String) = {

    df.collect().foreach(p => {

      val fileName = folder + "/" + p.getLong(0).toString() + ".txt.ann";
      val writerAnn = new java.io.PrintWriter(fileName, "UTF-8");
      if (!p.isNullAt(1))
        writerAnn.write(p.getString(1));
      writerAnn.close();

      val fileNameTxt = folder + "/" + p.getLong(0).toString() + ".txt.txt";
      val writerTxt = new java.io.PrintWriter(fileNameTxt, "UTF-8");
      if (!p.isNullAt(2))
        writerTxt.write(p.getString(2));
      writerTxt.close();

    })
    logger.info("Exported data")

  }

  def split_map(value: String): Seq[Tuple2[Int, Int]] = {
    import scala.collection.mutable.ListBuffer

    var a = new ListBuffer[Tuple2[Int, Int]]()

    if (value == null) {
      null
    } else {
      val pattern = """([\s\n]*\n[\s\n]*)""".r
      var b = 0
      var e = value.size
      pattern.findAllIn(value).matchData foreach {
        m => {
          e = m.start(1)
          if (b != e)
            a += Tuple2(b, e)
          b = m.end(1)
        }
      }
      if (b != value.size)
        a += Tuple2(b, value.size)
    }
    a
  }
}
