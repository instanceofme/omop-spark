/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.omop.deid

import com.typesafe.scalalogging.LazyLogging
import io.delta.tables.DeltaTable
import org.apache.spark.sql.functions.{col, rand, hash}
import org.apache.spark.sql.{DataFrame, SparkSession}


/**
 * Merge two tables based on a given column
 */
object Merge extends App with DeidIO with LazyLogging {

  val mode = args(0)
  val database = args(1)
  val tableFinal = args(2)
  val tableTmp = args(3)
  val columnJoin = args(4)
  val databaseHash = args(5)
  val tableTmpHash = args(6)

  require(tableFinal != tableTmp, "final and source table shall be different !")

  val spark = SparkSession
    .builder()
    .appName("OMOP Merge %s".format(tableFinal))
    .enableHiveSupport()
    .getOrCreate()

  import spark.implicits._

  spark.sparkContext.setLogLevel("WARN")

  // Read the candidate table (new annotation generated )
  val tmp = readDataframe(spark, mode, database, tableTmp)

  // calculate the hash for each note to discover the note that had changed
  val noteHash = readDataframe(spark, mode, databaseHash, tableTmpHash)
    .select('note_id, hash('note_text) as "note_hash")

  // procuce the candidate table with the hash
  val candidate = tmp.join(noteHash, Seq("note_id"), "left")

  logger.warn("Candidate count:%d".format(candidate.count()))

  //mode match {
  //  case "hive" => hiveMerge(candidate, database, tableFinal, columnJoin)
  //  case "delta" => deltaMerge(candidate, database, tableFinal, columnJoin)
  //  case _ => throw new Exception("Only hive and delta are supported for merge")
  //}
  // only delta implemented yet
  deltaMerge(candidate, database, tableFinal, columnJoin)
  def hiveMerge(candidate: DataFrame, database: String, tableFinal: String, columnJoin: String) = {

    val oldRows = readDataframe(spark, mode, database, tableFinal)
    writeDataframe(oldRows.union(candidate), mode, database, tableFinal + "_new")

    // switch new and old table
    // more secure than append to dataset
    candidate.sparkSession.sql("use %s".format(database))
    candidate.sparkSession.sql(f"alter table $tableFinal rename to ${tableFinal}_old")
    candidate.sparkSession.sql(f"alter table ${tableFinal}_new rename to ${tableFinal}")
    candidate.sparkSession.sql(f"drop table ${tableFinal}_old")

  }

  def deltaMerge(candidate: DataFrame, path: String, file: String, key: String) = {
    // TODO: if the Final table does not exists yet, create it
    val table = path + "/" + file
    logger.warn("Merging %s".format(table))

    val dt = DeltaTable.forPath(table)
    logger.warn("Source table size:%d".format(dt.toDF.count()))

    // first delete the note  that will be added
    dt.as("t")
      .merge(
        candidate.as("c").dropDuplicates(key.split(",")),
        generateKeyJoin(key, "t", "c")
      )
      .whenMatched().delete()
      .execute()
    logger.warn("Deleted")

    // second add the rows
    dt.as("t")
      .merge(
        candidate.as("c"),
        generateKeyJoin(key, "t", "c")
      )
      .whenNotMatched().insertAll()
      .execute()
    logger.warn("Inserted")

  }

  def generateKeyJoin(cols: String, left: String, right: String) = {
    val res = cols.split(",").map(x => s"${left}.$x=${right}.$x").mkString("", " AND ", "")
    logger.warn(res)
    res
  }

}

/**
 * Given a <note> table, this produces a <outputNote>
 * table given the existing <inputNoteNlpDeid> that
 * have already been deidentified.
 *
 * As a side effect, every note that do not contain any
 * personnal information will be resubmitted to the algorithm.
 *
 */
object Subset extends App with DeidIO with LazyLogging {

  val mode = args(0)
  val database = args(1)
  val source_database = args(2)
  val inputNote = args(3)
  val inputNoteNlpDeid = args(4)
  val outputNote = args(5)
  require(inputNote != outputNote, "final and source table shall be different !")

  val spark = SparkSession
    .builder()
    .appName("OMOP Subset %s".format(inputNote))
    .enableHiveSupport()
    .getOrCreate()

  import spark.implicits._

  val note = readDataframe(spark, mode, source_database, inputNote)
    .filter(!'note_text.isNull) // we dont process the empty notes
    .filter(col("note_event_id").isNull) // only deidentify note with no internal link
    .repartition(1000, col("note_id"))
    .withColumn("note_hash", hash('note_text))

  val noteNlpDeid = readDataframe(spark, "delta", database, inputNoteNlpDeid)
    .repartition(1000, col("note_id"))

  val subset = note.as("n")
    .join(
      noteNlpDeid.as("d"),
      col("n.note_id") === col("d.note_id")
        && col("n.note_hash") === col("d.note_hash"), "left_anti") //if content changed, reprocess it
    .sort(rand())

  writeDataframe(subset, mode, database, outputNote)

}
