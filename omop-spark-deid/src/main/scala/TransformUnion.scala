/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.omop.deid

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, SparkSession}

object TransformUnion extends DeidTables with DeidIO with App {

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Quality")
    .enableHiveSupport()
    .getOrCreate()


  process(spark)

  /**
   * Take both results from UIMA and GPU, and merge them.
   * It assumes UIMA is the truth over GPU.
   *
   * @param spark String Name of the hive database
   * @note TABLE INPUT: [[DEID_UIMA_TABLE]], [[DEID_GPU_TABLE]]
   * @note TABLE OUTPUT:  [[DEID_NOTE_NLP_QUALITY_TMP]]
   *
   */
  def process(spark: SparkSession) = {

    val noteNlpSpark = readTempDataframe(spark, DEID_UIMA_TABLE)
    val noteNlpGpu = readTempDataframe(spark, DEID_GPU_TABLE)

    val result = removeDuplicate(noteNlpSpark, noteNlpGpu)

    writeTempDataframe(result, DEID_NOTE_NLP_QUALITY_TMP)

  }

  def removeDuplicate(sparkDf: DataFrame, gpuDf: DataFrame): DataFrame = {
    val spark = sparkDf.sparkSession
    sparkDf.dropDuplicates("person_id", "note_id", "offset_begin", "offset_end", "note_nlp_source_value")
      .createOrReplaceTempView("uima")

    gpuDf.dropDuplicates("person_id", "note_id", "offset_begin", "offset_end", "note_nlp_source_value")
      .createOrReplaceTempView("gpu")

    DFTool.applySchemaSoft(
      spark.sql(
        """
      select g.person_id
      , g.note_id
      , g.snippet
      , g.offset_begin
      , g.offset_end
      , g.note_nlp_source_value
      , g.nlp_system
      , g.term_temporal
      from gpu        g
      left join uima s on
            s.person_id = g.person_id 
        and s.note_id = g.note_id
        and (  g.offset_begin between s.offset_begin and s.offset_end
            or g.offset_end between s.offset_begin and s.offset_end  )
      where s.note_id is null
            
            UNION ALL
            
      select g.person_id
      , g.note_id
      , g.snippet
      , g.offset_begin
      , g.offset_end
      , g.note_nlp_source_value
      , g.nlp_system
      , g.term_temporal
      from uima        g
          
      
      """), schemaNoteNlp)

  }

}

