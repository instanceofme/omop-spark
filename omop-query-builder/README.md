# Fhir Query Builder

## Principle

- get a query as input
- produces a list of FHIR lists as result

### Extended FHIR Syntax 

``` 
(Patient?gender=female&birthdate>=2010)[patient]
AND(Patient?gender=female&birthdate>=2010, Condition?coding=xxx&start-date=2020)[patient,condition]
```

### Query Builder FHIR Syntax 

``` 
AND(Patient?<solr-filter>, Composition?<solr-filter>, Condition?<solr-filter>)[patient]
OR(Patient?<solr-filter>, Composition?<solr-filter>, Condition?<solr-filter>)[patient,composition]
BEFORE(Composition?<solr-filter>, "start-date", Condition?<solr-filter>, "end-date", "1 day")[condition,composition]
```

### Complete Example

The below example asks for "lists of patients and their conditions for
female born after 2010 having a condition xxx in 2020". It returns
both patient and condition list ids and their count, that can be
fetched thougth FHIR List endpoint by the client.

```bash
# Fhir extended
AND(Patient?gender=female&birthdate>=2010, Condition?coding=xxx&start-date=2020)[patient,condition]

# Query Builder
AND(Patient?gender=female AND birthdate>=20100101, Condition?coding=xxx AND start-date >= 20200101 AND start-date <= 20201231)[patient,condition]
``` 

```scala 
# Spark code
val result = AND(readSolr(collection="Patient", filter="gender=female AND birthdate>=20100101", fields="patient_id"), readSolr(collection="Condition", filter="coding=xxx AND start-date >= 20200101 AND start-date <= 20201231", fields="patient_id,condition_id")[patient,condition]
writeSolr(collection="List", data=result)
returnClient(jsonFormat(result))
 ```

```json
# Returned value
{"patient": {"list_id":23, "count":500}, "condition": {"list_id":24,"count":1002}}
```

```

### Fhir syntax to Solr syntax

The `Fhir extended` syntax is translated into the `Query builder` syntax thougth thie FHIR API. Each FHIR ressource (Patient, Condition...) endpoint generates the Solr Filter and replaces the fhir Filter with (eg: `coding=xxx&start-date=2020` into  `coding=xxx AND start-date >= 20200101 AND start-date <= 20201231`)

# Fhir Stats Component

Given a list the component will return precomputed results. Those
precomputed metrics will be precalculed within an optimized data
structure.

For example, given a list of patient, returns aggregated values.
