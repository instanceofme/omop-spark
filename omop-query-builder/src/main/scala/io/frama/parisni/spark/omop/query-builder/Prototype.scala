package io.frama.parisni.spark.omop.query

import java.sql.Timestamp

import org.apache.spark.sql.SparkSession

// a patient
case class Patient(patient_id: Long, death_date: Timestamp)

// an encounter (= un sejour hospitalier -> plusieurs par patients)
case class Encounter(patient_id: Long, encounter_id: Long, start_date: Timestamp)

// une prise de médicament / realisé sur un patient lors d´un sejour
case class Drug(drug_id: Long, patient_id: Long, encounter_id: Long, exec_date: Timestamp)

// un diagnostic de maladie / realisé sur un patient lors d´un sejour
case class Condition(condition_id: Long, patient_id: Long, encounter_id: Long, exec_date: Timestamp)

object Prototype extends App {

  val spark = SparkSession.builder()
    .appName("Cohort 360 Sync")
    .getOrCreate()


  /*
   *  The data after being fetched from solr.
   *  SOLR is responsible to apply filters
   *  SPARK pushes down the filters to SOLR.
   *  SOLR has many many fields to filter on.
   *  ONLY the keys and dates are retrieved
   *  on the spark side from SOLR.
   */

  val patient = spark.createDataFrame(
    Patient(1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Patient(2L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Patient(3L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Nil
  )

  val encounter = spark.createDataFrame(
    Encounter(1L, 1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Encounter(1L, 2L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Encounter(2L, 1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Nil
  )

  val condition = spark.createDataFrame(
    Condition(100L, 1L, 1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Condition(1001L, 1L, 2L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Condition(1002L, 2L, 1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Nil
  )

  val drug = spark.createDataFrame(
    Drug(200L, 1L, 1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Drug(2001L, 1L, 2L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Drug(2002L, 2L, 1L, Timestamp.valueOf("2020-04-15 23:31:15"))
      :: Nil
  )

  // PATIENT LEVEL
  // Q: a patient having a drug and a condition
  // getPatient(AND(drug, condition))
  // -->  [1, 2]

  // Q: a patient having a drug and but no condition
  // getPatient(EXCEPT(drug, condition))

  // Q: a patient having a drug 10 days before a condition
  // getPatient(BEFORE(drug, 'start-date', condition, 'start-date',"10 day"))

  // Q: a patient having encounter 1 hour before deathdate
  // getPatient(BEFORE(patient, 'death-date', condition, 'start-date',"1 hour"))

  // ENCOUNTER LEVEL
  // Q: a patient having a drug and a condition in the same encounter
  // getPatient(AND_ENCOUNTER(drug, condition))

  // Q: a patient having a drug 10 days before a condition within the same encounter
  // getPatient(BEFORE_ENCOUNTER(drug, 'start-date', condition, 'start-date',"10 day"))

  // FETCH OTHER OBJECT THAN PATIENT
  // Q: the drugs of a patient having a drug and a condition
  // getDrug(AND(drug, condition))

  // Q: the conditions of a patient having a drug 10 days before a condition
  // getCondition(BEFORE(drug, 'start-date', condition, 'start-date',"10 day"))

  // Q: the encounters of a patient having a drug and but no condition
  // getEncounter(EXCEPT(drug, condition))

  // Q: get patient, encounter, drug and conditions from a patient having a drug and a condition in the same encounter
  // getPatientEncounterDrugAndCondition(AND_ENCOUNTER(drug, condition))

}
