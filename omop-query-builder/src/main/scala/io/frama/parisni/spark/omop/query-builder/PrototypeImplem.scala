import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import java.sql.Timestamp


/**
  Syntaxe:
  - 
  Plusieurs Objets:
  - Ressources (des dataframes d1, d2, d3... avec [fact_id, person_id, encounter_id, date] )
  - Operateurs (retournent des dataframes)
  - Query (Combinaisons d´opérateurs)
  - Result (un dataframe[id, type])
 Listes demandées:
  - patients
  - encounter
  - Composition, Measurement, Procedure, Condition ...
 Optimisations:
  - seules ressources impliquées dans operateurs temporels récupérées avec dates
  - si liste de retour demandé = encounter_id, ne pas recupérer person_id
  - si plusieurs listes de retour, alors mettre en cache la Query
 Problématique:
  - prevalence de operateurs
  - distributivité de l´opérateur temporel
  - produits carthesiens 
  **/

case class InputRow(is_orbis: Boolean, id_pdf: Long, lib_klasse: String, lib_doc: String, text: String)

def unionPro(DFList: List[DataFrame]): DataFrame = {
  val spark = DFList(0).sparkSession
    val MasterColList: Array[String] = DFList.map(_.columns).reduce((x, y) => (x.union(y))).distinct
    def unionExpr(myCols: Seq[String], allCols: Seq[String]): Seq[org.apache.spark.sql.Column] = {
      allCols.toList.map(x => x match {
        case x if myCols.contains(x) => col(x)
        case _ => lit(null).as(x)
      })
    }
    val masterSchema = StructType(DFList.map(_.schema.fields).reduce((x, y) => (x.union(y))).groupBy(_.name.toUpperCase).map(_._2.head).toArray)
    val masterEmptyDF = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], masterSchema).select(MasterColList.head, MasterColList.tail: _*)
    DFList.map(df => df.select(unionExpr(df.columns, MasterColList): _*)).foldLeft(masterEmptyDF)((x, y) => x.union(y))
}

/**
  OPERATORS
**/
def OR(df:DataFrame*) = {unionPro(df.toList)} /*OR(A, B)*/
def AND(df:DataFrame*) = {df.reduce((df1,df2) => (df1.drop("encounter_id","date").join(df2.drop("encounter_id","date"), Seq("person_id"))))} /*AND(A, B, C, D)*/
def EXCEPT(df:DataFrame, df2:DataFrame) = {df.join(df2,Seq("person_id"), "left_anti")} /*EXCEPT(A, B)*/
// temporal operators: MUST have date on both side
def BEFORE(df1:DataFrame, df2:DataFrame, delay:String) = {df1.as("df1").join(df2.as("df2"), Seq("person_id")).filter(s"df1.date + INTERVAL $delay < df2.date").drop(col("df2.person_id")).drop(col("df2.encounter_id")).drop(col("df2.date"))}/*BEFORE(A, B, "1 day")*/
def AFTER(df1:DataFrame, df2:DataFrame, delay:String) = {BEFORE(df2, df1, delay)}/*AFTER(A, B, "1 day")*/

// encounter: happens  within the same encounter
def AND_ENCOUNTER(df:DataFrame*) = {df.reduce((df1,df2) => (df1.as("df1").join(df2, Seq("person_id","encounter_id")))).drop("date")}
def EXCEPT_ENCOUNTER(df:DataFrame, df2:DataFrame) = {df.join(df2, Seq("encounter_id"), "left_anti")}
// temporal operators: MUST have date on both side
def BEFORE_ENCOUNTER(df1:DataFrame, df2:DataFrame, delay:String) = {df1.join(df2, Seq("encounter_id")).filter(s"df1.date + INTERVAL $delay < df2.date")}
def AFTER_ENCOUNTER(df1:DataFrame, df2:DataFrame, delay:String) = {BEFORE_ENCOUNTER(df2, df1, delay)}

def EXCEPT_FACT(df:DataFrame, df2:DataFrame) = {df.join(df2, Seq("fact_id"), "left_anti")}

def getPatient(df:DataFrame) = {df.selectExpr("person_id id","'person' as type").dropDuplicates("id")}
def getEncounter(df:DataFrame) = {df.selectExpr("encounter_id id","'encounter' as type").dropDuplicates("id")}
def getFact(df:DataFrame, id:Int) = {df.selectExpr(s"fact_id$id as id",s"'fact$id' as type").dropDuplicates("id")}

import spark.implicits._
val d1 = List((1,2,10,Timestamp.valueOf("2020-02-03 10:11:13")),(1,3,12,Timestamp.valueOf("2020-02-03 10:11:13"))).toDF("person_id","encounter_id","fact_id1","date")
val d2 = List((1,2,11,Timestamp.valueOf("2020-02-05 10:11:13")),(1,3,13,Timestamp.valueOf("2020-02-03 10:11:13"))).toDF("person_id","encounter_id","fact_id2","date")
val d3 = List((1,2,13,Timestamp.valueOf("2020-02-07 10:11:13")),(1,3,14,Timestamp.valueOf("2020-02-03 10:11:13"))).toDF("person_id","encounter_id","fact_id3","date")
val d4 = List((1,2,13,Timestamp.valueOf("2020-02-07 10:11:13")),(1,3,14,Timestamp.valueOf("2020-02-03 10:11:13"))).toDF("person_id","encounter_id","fact_id4","date")


// ex1: (d1 ou d2) (patients)
getPatient(OR(d1, d2))
// ex2: (d1 et d2) (patients)
getPatient(AND(d1, d2))
// ex2: (d1 et d2 et d3) (patients)
getPatient(AND(d1, d2, d3))
// ex2: (d1 ou d2 et d3) (patients)
getPatient(OR(AND(d2, d3), d1))
// ex2: (d1 after d2) (patients)
getPatient(BEFORE(d1, d2, "1 day"))
// t1 >> t2 >> t3
// before(t1, before(t2, t3))
getPatient(BEFORE(d1, BEFORE(d2, d3, "1 day"), "1 day"))
// (t1 >> t2 >> t3) and t4
// before(t1, before(t2, t3))
getPatient(AND(BEFORE(d1, BEFORE(d2, d3, "1 day"), "1 day"), d3))
// (t1 and t2) >> t3 >> t4
// (t1 >> t3 >> t4 and t2 >> t3 >> t4)


// Création d'une REQUETE (on peut extraire differentes listes)
val query = AND_ENCOUNTER(BEFORE(d1, BEFORE(d2, d3, "1 day"), "1 day"), d4)

// +---------+------------+--------+--------+--------+--------+
// |person_id|encounter_id|fact_id1|fact_id2|fact_id3|fact_id4|
// +---------+------------+--------+--------+--------+--------+
// |        1|           3|      12|      11|      13|      14|
// |        1|           2|      10|      11|      13|      13|
// +---------+------------+--------+--------+--------+--------+

// RECUPERER LA LISTE DES PATIENTS
val result = unionPro(
  getPatient(query)::
    Nil)

// +---+------+
// | id|  type|
// +---+------+
// |  1|person|
// +---+------+

// RECUPERER PLUSIEURS LISTES
val result = unionPro(
  getPatient(query)::
  getEncounter(query)::
  getFact(query,1)::
  getFact(query,2)::
  getFact(query,3)::
  getFact(query,4)::
    Nil)


// +---+---------+                                                                 
// | id|     type|
// +---+---------+
// |  1|   person|
// |  3|encounter|
// |  2|encounter|
// | 12|    fact1|
// | 10|    fact1|
// | 11|    fact2|
// | 13|    fact3|
// | 13|    fact4|
// | 14|    fact4|
// +---+---------+



val lab = List(
  (1,2,10)
    ,(1,3,12)
    ,(1,3,13)
).toDF("person_id","encounter_id","fact_id1")

val diag = List(
  (1,2,101)
    ,(1,3,121)
    ,(1,3,131)
).toDF("person_id","encounter_id","fact_id2")

val query = AND(lab, diag.dropDuplicates("person_id"))


val lab = List(
  (1,2,10,Timestamp.valueOf("2020-02-03 10:11:13"))
    ,(1,3,12,Timestamp.valueOf("2020-02-03 10:11:13"))
    ,(1,3,13,Timestamp.valueOf("2020-02-03 10:11:13"))
).toDF("person_id","encounter_id","fact_id1","date")

val diag = List(
  (1,2,101,Timestamp.valueOf("2020-02-05 10:11:13"))
    ,(1,3,121,Timestamp.valueOf("2020-02-06 10:11:13"))
    ,(1,3,131,Timestamp.valueOf("2020-02-06 10:11:13"))
).toDF("person_id","encounter_id","fact_id1","date")

val result = BEFORE(lab, diag, "1 day")
