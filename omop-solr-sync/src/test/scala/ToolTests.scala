/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.types._

class ToolTests extends QueryTest with SparkSessionTestWrapper  with OmopTransform {


  val schema1 = StructType(
    StructField("id", IntegerType) ::
      Nil
  )
  val schema2 = StructType(
    StructField("cd", IntegerType) ::
      Nil
  )

  val schema3 = StructType(
    StructField("lib", IntegerType) ::
      StructField("attribute", StringType) ::
      Nil
  )

  test("test union StructType") {

    val schemaResult = StructType(
      StructField("id", IntegerType) ::
        StructField("cd", IntegerType) ::
        StructField("lib", IntegerType) ::
        StructField("attribute", StringType) ::
        Nil
    )
    new T {
      assert(schemaUnion(schema1, schema2, schema3) == schemaResult)
    }
  }

  test("test union order StructType") {

    val schemaResult = StructType(
      StructField("id", IntegerType) ::
        StructField("lib", IntegerType) ::
        StructField("cd", IntegerType) ::
        StructField("attribute", StringType) ::
        Nil
    )
    new T {
      assert(schemaUnion(schema1, schema2, schema3) != schemaResult)
    }
  }

  test("test prepend columns") {
    new T {

      import spark.implicits._

      val resultSchema = StructType(StructField("patient.id", IntegerType, false) :: Nil)
      val tmp = (1 :: 2 :: Nil).toDF("id")
      assert(prependColumnPatient(tmp).schema == resultSchema)
    }
  }


  test("test gt") {}

}
