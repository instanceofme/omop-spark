/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import java.sql.{Date, Timestamp}

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{QueryTest, Row, SparkSession}
import org.apache.spark.sql.functions._

class RessourceTest extends QueryTest with SparkSessionTestWrapper with T with LazyLogging {

  val concept_fhir = (Row(2, new Timestamp(1L), "http://local", "Homme", "eche omo", "http://fhir", "Male") ::
    Row(2002861904, new Timestamp(1L), "http://local", "ipp", "Identifiant Patient Partagé", "http://fhir", "identifier") ::
    Row(2002861902, new Timestamp(1L), "http://local", "secu", "Numéro de sécurité sociale", "http://fhir", "identifier") ::
    Row(37, new Timestamp(1L), "http://local", "Supprimé", "Supprimé", "http://fhir", "entered-in-error") ::
    Row(3, new Timestamp(1L), "URG", "http://bob", "urg", "Urgence", "http://hl7") ::
    Nil
    , conceptFhirSchema)

  val person_secu = (Row(1L, Array(2L, 3L, 2, 4, 5)) ::
    Row(2L, Array(2L, 3L)) ::
    Nil, personSecuSchema)

  val cohort_list = (Row(1L, new Timestamp(1L), Array(2L, 3L, 2, 4, 5)) ::
    Row(2L, new Timestamp(1L), Array(2L, 3L)) ::
    Nil, cohortListSchema)

  def getDf(spark: SparkSession, t: Tuple2[List[Row], StructType]) = {
    spark.createDataFrame(spark.sparkContext.makeRDD(t._1), t._2)
  }

  test("test mapper concept") {
    val sqlCtx = spark.sqlContext

    sqlCtx.setConf("spark.sql.crossJoin.enabled", "true")

    val date = "CAST(UNIX_TIMESTAMP('08/26/2016', 'MM/dd/yyyy') AS TIMESTAMP)"

    val concept = spark.sql(
      f"""
  select cast(1 as long) concept_id                                                                                                                                                                                                                                                                                                                           
  , 'my concept label' concept_name                                                                                                                                                                                                                                                                                                                              
  , 'Observation' domain_id                                                                                                                                                                                                                                                                                                                                 
  , 'SNOMED' vocabulary_id                                                                                                                                                                                                                                                                                                                             
  , cast(null as string) concept_class_id                                                                                                                                                                                                                                                                                                                          
  , 'S' standard_concept                                                                                                                                                                                                                                                                                                                          
  , 'XX1' concept_code                                                                                                                                                                                                                                                                                                                              
  , 'EN' m_language_id                                                                                                                                                                                                                                                                                                                             
  , 1 m_project_id                                                                                                                                                                                                                                                                                                                              
  , cast(null as string) invalid_reason                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
  """)

    val project = spark.sql(
      f"""
  select                                                                                                                                                                                                                                                                                                                                      
      1  m_project_id                                                                                                                                                                                                                                                                                                                             
    , 'APHP' m_project_type_id                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
  """)

    val stats_avg = spark.sql(
      f"""
  select 
    cast(1 as long) concept_id                                                                                                                                                                                                                                                                                                           
  , cast(3.4 as double) m_value_avg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
  """)

    val stats_freq = spark.sql(
      f"""
  select 
    cast(1 as long) concept_id                                                                                                                                                                                                                                                                                                           
  , cast(4 as double) as m_frequency_value                                                                                                                                                                                                                                                                                             
  """)

    val concept_synonym = spark.sql(
      """
  select                                                                                                                                                                                                                                                                                                                                      
    cast(1 as long) concept_id                                                                                                                                                                                                                                                                                                                                
  , 'my other concept label' concept_synonym_name                                                                                                                                                                                                                                                                                                                      
  , cast(12313 as long) language_concept_id                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
  """)

    val concept_relationship = spark.sql(
      f"""
  select                                                                                                                                                                                                                                                                                                                                      
    cast(1 as long) concept_id_1                                                                                                                                                                                                                                                                                                                              
  , cast(2 as long) concept_id_2                                                                                                                                                                                                                                                                                                                              
  , 'is a' relationship_id                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
  """)

    val output = sqlCtx.sql(
      f"""
          select 
              cast(1  as long)                           as id
            , cast($date as timestamp)         as `_lastUpdated`
            , cast($date as timestamp)         as `birthdate`
            , array() as identifier
            , array('bob')                as given
            , array() as family
          """)

    val input = MapperConceptSync.transform(concept, project, stats_avg, stats_freq, concept_synonym, concept_relationship)
    input.show()
    input.printSchema

  }

  test("case when") {
    val pivotColumns = Map("given" -> List(2002861901), "family" -> List(2002861897), "email" -> List(2002861903))

    print(FHIRPatientSolrSync.generateCaseWhen("observation_source_concept_id", pivotColumns))
  }

  test("case filter") {
    val pivotColumns = Map("given" -> List(2002861901), "family" -> List(2002861897), "email" -> List(2002861903))

    print(FHIRPatientSolrSync.generateFilter("observation_source_concept_id", pivotColumns))
  }


  test("test fhir organization") {
    val care_site = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), "URG", "Urgences Psy", "URG Psy", 3
        , new Date(1L)) ::
        Nil), careSiteSchema)

    val fact_relationship = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), 2L, 57) ::
        Nil
    ), schema = factRelationshipSchema)

    FHIROrganizationSolrSync.transform(care_site, fact_relationship, getDf(spark, concept_fhir)).show(false)
  }

  test("test fhir practitionner") {

    val provider = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), "Bob", "L'eponge", 3, "1234", new Timestamp(2L)) ::
        Row(2L, new Timestamp(1L), "Jim", "Le poisson", 3, "1234", new Timestamp(2L)) ::
        Nil), schema = providerSchema)


    FHIRPractitionerSolrSync.transform(provider, getDf(spark, concept_fhir)).show(false)
  }

  test("test fhir condition") {

    val condition = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), 1L, 3L, 2, 4, 3, 5, new Timestamp(2L)) ::
        Row(2L, new Timestamp(1L), 2L, 3L, 2, 4, 3, 5, new Timestamp(2L)) ::
        Nil), schema = conditionSchema)

    FHIRConditionSolrSync.transform(condition, getDf(spark, concept_fhir)).show(false)
  }

  test("test fhir procedure") {

    val procedure = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), 1L, 3L, 2, 4, 5, new Timestamp(2L)) ::
        Row(2L, new Timestamp(1L), 2L, 3L, 2, 4, 5, new Timestamp(2L)) ::
        Nil), schema = procedureSchema)

    FHIRProcedureSolrSync.transform(procedure, getDf(spark, concept_fhir)).show(false)

  }

  test("test fhir encounter") {

    val visitDetail = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1), 1L, 21L, 2, 2, new Timestamp(1), new Timestamp(1), 1L, 2, 33L) ::
        Nil), schema = visitDetailSchema)

    val visitOccurrence = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1), "12345", new Timestamp(1), new Timestamp(1), 1L, 1, 21, 1L, 44L) ::
        Nil), schema = visitOccurrenceSchema)

    val observation = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(2L, new Timestamp(1L), 2002861899, "12343546") ::
        Nil), schema = observationVisitSchema)

    val care_site = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), "URG", "Urgences Psy", "URG Psy", 3
        , new Date(1L)) ::
        Nil), careSiteSchema)

    FHIREncounterSolrSync.transform(visitOccurrence, visitDetail, observation, getDf(spark, concept_fhir), care_site).printSchema()
  }

  test("test fhir patient") {

    val person = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1), 2, new Timestamp(1), new Timestamp(1), 2) ::
        Nil), schema = personSchema)

    val observation = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1L), 2002861901, "bob") ::
        Row(1L, new Timestamp(1L), 2002861901, "jim") ::
        Row(1L, new Timestamp(1L), 2002861897, "maurisson") ::
        Row(1L, new Timestamp(1L), 2002861904, "12345") ::
        Row(1L, new Timestamp(1L), 2002861902, "67000") ::
        Nil), schema = observationPersonSchema)

    val visitOccurrence = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1), "12345", new Timestamp(1), new Timestamp(1), 1L, 1, 21, 1L, 44L) ::
        Nil), schema = visitOccurrenceSchema)

    val cohort = spark.createDataFrame(spark.sparkContext.makeRDD(List(
      Row(1L, new Timestamp(1), 1L, 1L)
    )), cohortSchema)

    FHIRPatientSolrSync.transform(person, observation, visitOccurrence, getDf(spark, concept_fhir)).show(false)

  }


  test("test fhir claim") {

    val cost = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1), 2L, 33L, 1147624, new Timestamp(2), 43, 55) ::
        Nil), schema = costSchema)

    FHIRClaimSolrSync.transform(cost, getDf(spark, concept_fhir)).show(false)
    FHIRClaimSolrSync

  }

  test("test fhir explain") {

    val cost = spark.createDataFrame(spark.sparkContext.makeRDD(
      Row(1L, new Timestamp(1), 2L, 33L, 1147624, new Timestamp(2), 43, 55) ::
        Nil), schema = costSchema)
    import spark.implicits._
    val innerData = ((1L) :: Nil).toDF("patient")

    FHIRClaimSolrSync.transform(cost, getDf(spark, concept_fhir))
      .join(innerData, "patient")
      .explain()

    FHIRClaimSolrSync.transform(cost.join(innerData.withColumnRenamed("patient", "person_id"), "person_id"), getDf(spark, concept_fhir))
      .explain()

  }

  test("test fhir valueSet") {

    import spark.implicits._
    //spark.sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val concept = ((1, "xb28", "the concept", "bob")
      :: (2, "xb29", "the other concept", "jim")
      :: (3, "xb30", "the very other concept", "jim")
      :: Nil).toDF("concept_id", "concept_code", "concept_name", "vocabulary_id")
      .withColumn("change_datetime", current_timestamp())

    val fact_relationship = ((1, 2, "Contains")
      :: (1, 3, "Contains")
      :: (3, 1, "Contained in")
      :: (1, 2, "Subsumes")
      :: (2, 1, "Is a")
      :: Nil).toDF("concept_id_1", "concept_id_2", "relationship_id")
      .withColumn("change_datetime", current_timestamp())

    val vocabulary = (
      ("bob", "http://bob")
        :: ("jim", "http://jim")
        :: Nil).toDF("vocabulary_id", "vocabulary_reference")
      .withColumn("change_datetime", current_timestamp())

    FHIRValueSetSolrSync.transform(concept, fact_relationship, vocabulary).show(false)

  }

}

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}
