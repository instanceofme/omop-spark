/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.types._

class FHIRPatientSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None)
  extends SolrSync(spark, options, url, deltaPath, delay) {

  _collection = "patientAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {

    logger.warn("Generating %s from OMOP".format(_collection))
    val person = spark.read.format("delta").load(deltaPath + "person")
    val observation = spark.read.format("delta").load(deltaPath + "observation")
    val visitOccurrence = spark.read.format("delta").load(deltaPath + "visit_occurrence")

    this.dfTransformed = FHIRPatientSolrSync.transform(person.repartition(_partitionNumMedium), observation, visitOccurrence, concept_fhir)

    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRPatientSolrSync.outputSchema)
    writeCollection()
    this

  }
}

object FHIRPatientSolrSync extends T {


  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("patient", LongType)
      :: StructField("_lastUpdated", TimestampType)
      :: StructField("birthdate", TimestampType)
      :: StructField("birthdate-facet", IntegerType)
      :: StructField("death-date", TimestampType)
      :: StructField("death-date-facet", IntegerType)
      :: StructField("deceased", BooleanType)
      :: StructField("active", BooleanType)
      :: StructField("identifier", ArrayType(StringType))
      :: StructField("identifier-simple", StringType)
      :: StructField("gender", ArrayType(StringType))
      :: StructField("gender-simple", StringType)
      :: StructField("given", ArrayType(StringType))
      :: StructField("given-simple", StringType)
      :: StructField("family", ArrayType(StringType))
      :: StructField("family-simple", StringType)
      :: StructField("age-month", IntegerType)
      :: StructField("age-month-last-visit", IntegerType)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRPatientSolrSync = {
    val a = new FHIRPatientSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    a
  }

  val columns = Map("given" -> List(2002861901), "family" -> List(2002861897), "identifier" -> List(2002861902, 2002861904))

  def transform(person: DataFrame, observation: DataFrame, visitOccurrence: DataFrame, conceptFhir: DataFrame): DataFrame = {

    val personDf = DFTool.applySchemaSoft(person, personSchema)
    val observationDf = DFTool.applySchemaSoft(observation, observationPersonSchema)
    val visitOccurrenceDf = DFTool.applySchemaSoft(visitOccurrence, visitOccurrenceSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(conceptFhir, conceptFhirSchema)

    val filteredObs = observationDf.as("o")
      .filter(expr(generateFilter("observation_source_concept_id", columns)))

    val window =
      """row_number() over(
        |partition by person_id
        |order by
        |  coalesce(visit_end_datetime, case when length(visit_occurrence_source_value) > 9 then visit_start_datetime else null end) desc nulls first
        |, length(visit_occurrence_source_value) asc
        |, visit_start_datetime desc)""".stripMargin

    val last_visit = visitOccurrenceDf.withColumn("index_visit", expr(window))
      .filter("index_visit = 1")
      .select("person_id", "visit_start_datetime")


    val retObs = DFTool.simplePivot(
      filteredObs
        .join((conceptFhirDf).alias("i"), col("observation_source_concept_id") === col("i.concept_id"), "left")
        .withColumn("key", expr(generateCaseWhen("observation_source_concept_id", columns)))
        .withColumn("value",
          when(col("key").isin("given", "family"), array(col("value_as_string"))).otherwise(
            when(col("key").isin("identifier"), fhirTokenWithValueArray("i", "value_as_string"))))
        .groupBy(col("person_id"), col("key"))
        .agg(flatten(collect_list(col("value"))).as("value")),
      groupBy = col("person_id"), key = col("key"), aggCol = "value", _levels = columns.keySet.toList)
      .join(filteredObs.as("f").groupBy(col("person_id")).agg(max(col("change_datetime")).as("change_datetime")),
        "person_id" :: Nil, "left")

    //   *la répartition des âges des patients à la dernière prise en charge séparé par genre
    //  age en mois a la derniere prise en charge / join sur visit_occurrence
    val retPatient = getLastUpdatedDf("f" :: "g" :: Nil,
      personDf.alias("f")
        .join(last_visit, Seq("person_id"), "left")
        .join((conceptFhirDf).alias("g"), col("f.gender_source_concept_id") === col("g.concept_id"), "left")
        .withColumn("active", when(col("f.row_status_source_concept_id").isin(2008111363), lit(true)).otherwise(lit(false)))
        .withColumn("patient", col("person_id"))
        .withColumn("gender", fhirTokenArray("g"))
        .withColumn("birthdate-facet", year(col(("birth_datetime"))))
        .withColumn("death-date-facet", year(col(("death_datetime"))))
        .withColumn("age-month", expr("cast(months_between(coalesce(death_datetime,current_timestamp), birth_datetime) as int)"))
        .withColumn("age-month-last-visit", expr("cast(months_between(visit_start_datetime, birth_datetime) as int)"))
        .withColumn("gender-simple", fhirFacet("g")))
      .withColumn("deceased", when(col("death_datetime").isNotNull, lit(true)).otherwise(lit(false)))
      .select("person_id", "_lastUpdated",
        "gender", "gender-simple",
        "active", "birth_datetime",
        "death_datetime", "deceased",
        "birthdate-facet", "death-date-facet",
        "patient", "age-month", "age-month-last-visit")
      .withColumnRenamed("_lastUpdated", "change_datetime")

    val ret = getLastUpdatedDf("p" :: "r" :: Nil,
      retPatient.alias("p")
        .join(retObs.alias("r"), "person_id" :: Nil, "left"))
      .withColumn("identifier-simple", expr("identifier[0]"))
      .withColumn("given-simple", expr("given[0]"))
      .withColumn("family-simple", expr("family[0]"))
      .select(
        "p.person_id", "_lastUpdated",
        "gender", "active",
        "birth_datetime", "death_datetime", "deceased",
        "identifier", "identifier-simple", "given", "given-simple",
        "family", "family-simple", "patient",
        "gender-simple", "birthdate-facet",
        "death-date-facet", "age-month", "age-month-last-visit")

    val groupColumns = Map("person_id" -> "id", "birth_datetime" -> "birthdate", "death_datetime" -> "death-date")
    return DFTool.applySchemaSoft(DFTool.dfRenameColumn(ret, groupColumns), outputSchema)
  }
}

