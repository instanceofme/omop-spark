/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

/*

<field name="name" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="partof" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="type" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="active" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="identifier" type="text_general" indexed="true" stored="false" multiValued="true" />

 *
 */

class FHIRPractitionerSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "practitionerAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {


    logger.warn("Generating %s from OMOP".format(_collection))
    val provider = spark.read.format("delta").load(deltaPath + "provider")
    this.dfTransformed = FHIRPractitionerSolrSync.transform(provider, concept_fhir)
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRPractitionerSolrSync.outputSchema)
    writeCollection()


    this
  }
}

object FHIRPractitionerSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("identifier", ArrayType(StringType), true)
      :: StructField("identifier-simple", StringType, true)
      :: StructField("given", ArrayType(StringType), true)
      :: StructField("given-simple", StringType, true)
      :: StructField("family", ArrayType(StringType), true)
      :: StructField("family-simple", StringType, true)
      :: StructField("name", ArrayType(StringType), true)
      :: StructField("name-simple", StringType, true)
      :: StructField("active", TimestampType, true)
      :: StructField("gender", StringType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRPractitionerSolrSync = {
    val a = new FHIRPractitionerSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(fact: Dataset[Row], concept: Dataset[Row]): Dataset[Row] = {

    val spark = fact.toDF.sparkSession

    val factDf = DFTool.applySchemaSoft(fact, providerSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concept, conceptFhirSchema)

    val ret = getLastUpdatedDf("f" :: "v" :: Nil, factDf.alias("f")
      .join(conceptFhirDf.alias("v"), col("f.gender_source_concept_id") === col("v.concept_id"), "left")
      .withColumn("gender", fhirTokenArray("v"))
      .withColumn(
        "identifier",
        array(
          concat(
            lit("https://fhir.eds.aphp.fr/ConceptCode/practitioner-identifier"), lit("|"),
            lit("aphp-code"), lit("|"),
            col("provider_source_value"))))
      .withColumn("identifier-simple", col("provider_source_value"))
      .withColumn("name", array(concat(col("firstname"), lit(" "), col("lastname"))))
      .withColumn("name-simple", col("lastname"))
      .withColumn("given", array(col("firstname")))
      .withColumn("given-simple", col("firstname"))
      .withColumn("active", when(col("f.valid_end_datetime").isNull, lit(true)).otherwise(lit(false)))
      .withColumn("family", array(col("lastname")))
      .withColumn("family-simple", col("lastname"))
      .withColumnRenamed("provider_id", "id")
    )
      .select("id", "gender", "name", "name-simple", "given", "given-simple", "active", "_lastUpdated"
        , "family", "family-simple", "identifier", "identifier-simple")

    return DFTool.applySchemaSoft(ret, outputSchema)

  }
}
