/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

class FHIRConditionSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "conditionAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {

    logger.warn("Generating %s from OMOP".format(_collection))
    val condition = spark.read.format("delta").load(deltaPath + "condition_occurrence")
    this.dfTransformed = FHIRConditionSolrSync.transform(condition.repartition(_partitionNumLarge), concept_fhir)
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRConditionSolrSync.outputSchema)

    writeCollection(joinStr = "encounterAphp")

    this
  }
}

object FHIRConditionSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("encounter", LongType, true)
      :: StructField("encounter-type", StringType, true)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("recorded-date", TimestampType, true)
      //:: StructField("category", StringType, false)
      :: StructField("code", ArrayType(StringType), false)
      :: StructField("code-status", ArrayType(StringType), false)
      :: StructField("code-simple", StringType, false)
      :: StructField("verification-status", ArrayType(StringType), true)
      :: StructField("verification-status-simple", StringType, true)
      :: StructField("_text_", StringType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRConditionSolrSync = {
    val a = new FHIRConditionSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(facts: Dataset[Row], concepts: Dataset[Row]): DataFrame = {

    val conditionDf = DFTool.applySchemaSoft(facts, conditionSchema)
    val conceptFhir = DFTool.applySchemaSoft(concepts, conceptFhirSchema)

    val ret = getLastUpdatedDf("f" :: "c" :: "cs" :: Nil, conditionDf.alias("f")
      .join((conceptFhir).alias("c"), col("f.condition_source_concept_id") === col("c.concept_id"), "left")
      .join((conceptFhir).alias("cs"), col("f.row_status_source_concept_id") === col("cs.concept_id"), "left")
      .join((conceptFhir).alias("cd"), col("f.condition_status_source_concept_id") === col("cd.concept_id"), "left")
      .withColumn("verification-status", fhirTokenArray("cs"))
      .withColumn("verification-status-simple", fhirTokenSimple("cs"))
      .withColumn("code", fhirTokenArray("c"))
      .withColumn("code-simple", fhirTokenSimple("c"))
      .withColumn("code-status", fhirTokenArray("cd"))
      .withColumn("patient", col("f.person_id"))
      .withColumn("recorded-date", col("f.condition_start_datetime"))
      .withColumn("patient-type", lit("Patient"))
      .withColumn("encounter", col("visit_occurrence_id"))
      .withColumn("_text_", fhirNarrative("c", "cs", "cd"))
      .withColumn("encounter-type", lit("Encounter")))
      .select("condition_occurrence_id", "patient",
        "patient-type", "encounter",
        "encounter-type", "code", "code-simple","code-status",
        "verification-status","verification-status-simple", "_lastUpdated",
        "recorded-date", "_text_")

    val groupColumns = Map("condition_occurrence_id" -> "id")

    return DFTool.applySchemaSoft(DFTool.dfRenameColumn(ret, groupColumns), outputSchema)
  }

}
