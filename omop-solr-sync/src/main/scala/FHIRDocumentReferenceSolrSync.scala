/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

/*
 * La ressource FHIR procedure
<field name="date" type="pdates" indexed="true" stored="false" multiValued="true" />
<field name="identifier" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="instantiates" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="performer" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="subject" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="part-of" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="based-on" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="context" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="location" type="text_general" indexed="true" stored="false" multiValued="true" />


<field name="encounter" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="patient" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="category" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="status" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="code" type="text_general" indexed="true" stored="false" multiValued="true" />



 *
 */

class FHIRDocumentReferenceSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "documentReferenceAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {


    logger.warn("Generating %s from OMOP".format(_collection))
    val note = spark.read.format("delta").load(deltaPath + "note")
      .filter(col("note_event_field_concept_id").isNull)
    this.dfTransformed = FHIRDocumentReferenceSolrSync.transform(note.repartition(_partitionNumMedium), concept_fhir)

    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRDocumentReferenceSolrSync.outputSchema)
    writeCollection(joinStr = "encounterAphp")

    this
  }
}

object FHIRDocumentReferenceSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("encounter", LongType, true)
      :: StructField("encounter-type", StringType, true)
      :: StructField("description", StringType, true)
      :: StructField("date", TimestampType, true)
      :: StructField("category", ArrayType(StringType), true)
      :: StructField("category-simple", StringType, true)
      :: StructField("type", ArrayType(StringType), true)
      :: StructField("type-simple", StringType, true)
      :: StructField("status", ArrayType(StringType), true)
      :: StructField("status-simple", StringType, true)
      :: StructField("_text_", StringType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRDocumentReferenceSolrSync = {
    val a = new FHIRDocumentReferenceSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(fact: DataFrame, concept: DataFrame): DataFrame = {
    val spark = fact.toDF.sparkSession

    val factDf = DFTool.applySchemaSoft(fact, documentReferenceSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concept, conceptFhirSchema)

    val ret = getLastUpdatedDf("f" :: "v" :: "c" :: "cs" :: Nil, factDf.alias("f")
      .join((conceptFhirDf).alias("v"), col("f.note_type_source_concept_id") === col("v.concept_id"), "left")
      .join((conceptFhirDf).alias("c"), col("f.note_class_source_concept_id") === col("c.concept_id"), "left")
      .join((conceptFhirDf).alias("cs"), col("f.row_status_source_concept_id") === col("cs.concept_id"), "left")
      .withColumn("category", fhirTokenArray("v"))
      .withColumn("category-simple", fhirTokenSimple("v"))
      .withColumn("status", fhirTokenArray("cs"))
      .withColumn("status-simple", fhirTokenSimple("cs"))
      .withColumn("type", fhirTokenArray("c"))
      .withColumn("type-simple", fhirTokenSimple("c"))
      .withColumn("patient", col("f.person_id"))
      .withColumn("patient-type", lit("Patient"))
      .withColumn("encounter", col("visit_occurrence_id"))
      .withColumn("description", col("note_title"))
      .withColumn("_text_", fhirNarrative("c", "v", "cs"))
      .withColumn("encounter-type", lit("Encounter")))
      .withColumnRenamed("note_id", "id")
      .withColumnRenamed("note_datetime", "date")
      .select("id", "patient",
        "patient-type", "encounter",
        "encounter-type", "_lastUpdated",
        "category", "category-simple", "type", "type-simple", "date",
        "status", "status-simple", "_text_", "description")


    return DFTool.applySchemaSoft(ret, outputSchema)
  }
}
