/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}


class FHIREncounterSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "encounterAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {

    logger.warn("Generating %s from OMOP".format(_collection))
    var visitOccurrence = spark.read.format("delta").load(deltaPath + "visit_occurrence")
    var visitDetail = spark.read.format("delta").load(deltaPath + "visit_detail")
    var observation = spark.read.format("delta").load(deltaPath + "observation")
    var care_site = spark.sparkContext.broadcast(spark.read.format("delta").load(deltaPath + "care_site"))

    this.dfTransformed = FHIREncounterSolrSync.transform(visitOccurrence.repartition(_partitionNumLarge)
      , visitDetail.repartition(_partitionNumLarge), observation, concept_fhir, care_site.value)
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIREncounterSolrSync.outputSchema)

    writeCollection(joinStr = "patientAphp")

    this
  }
}

object FHIREncounterSolrSync extends T {
  val defaultNull = new MetadataBuilder().putNull("default").build
  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("encounter", LongType, false)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("part-of", LongType, true, defaultNull)
      :: StructField("part-of-type", StringType, true, defaultNull)
      :: StructField("type", ArrayType(StringType), true, defaultNull)
      :: StructField("type-simple", StringType, true, defaultNull)
      :: StructField("class", ArrayType(StringType), true, defaultNull)
      :: StructField("class-simple", StringType, true, defaultNull)
      :: StructField("status", ArrayType(StringType), true, defaultNull)
      :: StructField("status-simple", StringType, true, defaultNull)
      :: StructField("identifier", ArrayType(StringType), true, defaultNull)
      :: StructField("identifier-simple", StringType, true, defaultNull)
      :: StructField("start-date", TimestampType, true, defaultNull)
      :: StructField("end-date", TimestampType, true, defaultNull)
      :: StructField("isDetail", BooleanType, true, defaultNull)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("_text_", StringType, true)
      :: StructField("length", DoubleType, true)
      :: StructField("service-provider", LongType, true, defaultNull)
      :: StructField("service-provider-type", StringType, true, defaultNull)
      :: StructField("service-provider.display", StringType, true, defaultNull)
      :: StructField("start-date-facet", IntegerType, false)
      :: StructField("end-date-facet", IntegerType, false)
      :: StructField("start-date-month-facet", IntegerType, false)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIREncounterSolrSync = {
    val a = new FHIREncounterSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(visitOccurrence: DataFrame, visitDetail: DataFrame, observation: DataFrame, concepts: DataFrame, careSite:DataFrame): DataFrame = {

    val identifierColumns = Map("identifier" -> List(2002861899))
    val visitOccurrenceDf = DFTool.applySchemaSoft(visitOccurrence, visitOccurrenceSchema)
    val visitDetailDf = DFTool.applySchemaSoft(visitDetail, visitDetailSchema)
    val observationDf = DFTool.applySchemaSoft(observation, observationVisitSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concepts, conceptFhirSchema)
    val caresiteDf = DFTool.applySchemaSoft(careSite, careSiteSchema)

    val retIdentifier = getLastUpdatedDf("ob" :: "i" :: Nil, observationDf.as("ob")
      .filter(expr(generateFilter("ob.observation_source_concept_id", identifierColumns)))
      .join((conceptFhirDf).alias("i"), col("ob.observation_source_concept_id") === col("i.concept_id"), "left")
      .withColumn("value", fhirTokenWithValueArray("i", "value_as_string")))
      .groupBy("visit_occurrence_id")
      .agg(
        flatten(collect_list(col("value"))).alias("identifier")
        , max(col("_lastUpdated")).as("change_datetime"))
      .withColumn("identifier-simple", expr("identifier[0]"))


    //  *La répartition du nombre de visites par mois séparé par genre
    // month-visit
    val retOccurrence = getLastUpdatedDf("o" :: "i" :: "c" :: "t" :: "cs" :: Nil,
      visitOccurrenceDf.alias("o")
        .withColumn("encounter", col("o.visit_occurrence_id"))
        .join(caresiteDf.alias("csi"), col("o.care_site_id") === col("csi.care_site_id"), "left")
        .join(retIdentifier.alias("i"), col("i.visit_occurrence_id") === col("o.visit_occurrence_id"), "left")
        .join((conceptFhirDf).alias("c"), col("o.visit_source_concept_id") === col("c.concept_id"), "left")
        .join((conceptFhirDf).alias("t"), col("o.visit_type_source_concept_id") === col("t.concept_id"), "left")
        .join((conceptFhirDf).alias("cs"), col("o.row_status_source_concept_id") === col("cs.concept_id"), "left")
        .withColumn("service-provider", col("o.care_site_id"))
        .withColumn("service-provider.display", regexp_replace(lower(col("csi.care_site_name"))," ",""))
        .withColumn("service-provider-type", lit("Organization"))
        .withColumn("status", fhirTokenArray("cs"))
        .withColumn("status-simple", fhirTokenSimple("cs"))
        .withColumn("class", fhirTokenArray("c"))
        .withColumn("class-simple", fhirTokenSimple("c"))
        .withColumn("type", fhirTokenArray("t"))
        .withColumn("type-simple", fhirTokenSimple("t"))
        .withColumn("id", col("o.visit_occurrence_id").cast(StringType))
        .withColumn("patient", col("person_id"))
        .withColumn("patient-type", lit("Patient"))
        .withColumn("_text_", fhirNarrative("c", "t", "cs"))
        .withColumn("start-date-facet", year(col("visit_start_datetime")))
        .withColumn("end-date-facet", year(col("visit_end_datetime")))
        .withColumn("start-date-month-facet", month(col("visit_start_datetime")))
        .withColumnRenamed("visit_start_datetime", "start-date")
        .withColumnRenamed("visit_end_datetime", "end-date")
        .withColumn("length", datediff(col("end-date"), col("start-date")))
    )

    val retDetail = getLastUpdatedDf("d" :: "i" :: "c" :: "t" :: "cs" :: Nil,
      visitDetailDf.alias("d")
        .withColumn("encounter", col("d.visit_detail_id"))
        .join(caresiteDf.alias("csi"), col("d.care_site_id") === col("csi.care_site_id"), "left")
        .join(retIdentifier.alias("i"), col("i.visit_occurrence_id") === col("d.visit_occurrence_id"), "left")
        .join((conceptFhirDf).alias("c"), col("d.visit_detail_source_concept_id") === col("c.concept_id"), "left")
        .join((conceptFhirDf).alias("t"), col("d.visit_detail_type_source_concept_id") === col("t.concept_id"), "left")
        .join((conceptFhirDf).alias("cs"), col("d.row_status_source_concept_id") === col("cs.concept_id"), "left")
        .withColumn("service-provider", col("d.care_site_id"))
        .withColumn("service-provider-type", lit("Organization"))
        .withColumn("service-provider.display", col("csi.care_site_name"))
        .withColumn("status", fhirTokenArray("cs"))
        .withColumn("status-simple", fhirTokenSimple("cs"))
        .withColumn("class", fhirTokenArray("c"))
        .withColumn("class-simple", fhirTokenSimple("c"))
        .withColumn("type", fhirTokenArray("t"))
        .withColumn("type-simple", fhirTokenSimple("t"))
        .withColumn("part-of", coalesce(col("visit_detail_parent_id"), col("d.visit_occurrence_id")))
        .withColumn("part-of-type", lit("Encounter"))
        .withColumn("isDetail", lit(true))
        .withColumn("id", col("visit_detail_id").cast(StringType))
        .withColumn("patient", col("person_id"))
        .withColumn("patient-type", lit("Patient"))
        .withColumn("_text_", fhirNarrative("c", "t", "cs"))
        .withColumn("start-date-facet", year(col("visit_detail_start_datetime")))
        .withColumn("end-date-facet", year(col("visit_detail_end_datetime")))
        .withColumn("start-date-month-facet", month(col("visit_detail_start_datetime")))
        .withColumnRenamed("visit_detail_start_datetime", "start-date")
        .withColumnRenamed("visit_detail_end_datetime", "end-date")
        .withColumn("length", datediff(col("end-date"), col("start-date"))
        )
    )

    val ret = DFTool.applySchemaSoft(retOccurrence, outputSchema)
      .union(DFTool.applySchemaSoft(retDetail, outputSchema))

    return DFTool.applySchemaSoft(ret, outputSchema)

  }
}
