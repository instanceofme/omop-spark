/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}


class FHIRCompositionSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "compositionAphp"

  def transformDelta(concept_fhir: DataFrame, noteCalc: DataFrame): this.type = {


    logger.warn("Generating %s from OMOP".format(_collection))
    val note = spark.read.format("delta").load(deltaPath + "note")
    val concept = spark.read.format("delta").load(deltaPath + "concept")

    val sectionList = concept
      .filter(col("vocabulary_id") === lit("APHP - ORBIS - Document Textuel Hospitalier Section"))
      .select(col("concept_code"))
      .collect().map(r => "section_" + r.getString(0) + "_txt").toList

    this.dfTransformed = FHIRCompositionSolrSync.transform(note.repartition(_partitionNumLarge), concept_fhir, sectionList, noteCalc.repartition(_partitionNumLarge))
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRCompositionSolrSync.outputSchema)

    writeCollection(joinStr = "encounterAphp")

    this

  }

  def getSectionList(): List[String] = {
    this._pg.sqlExecWithResult(
      """
      select concept_code 
      from concept 
      where vocabulary_id = 'APHP - ORBIS - Document Textuel Hospitalier Section'
      """).collect().map(r => "section_" + r.getString(0) + "_txt").toList
  }

}

object FHIRCompositionSolrSync extends T {

  var outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("encounter", LongType, true)
      :: StructField("encounter-type", StringType, true)
      :: StructField("author", LongType, true)
      :: StructField("author-type", StringType, true)
      :: StructField("date", TimestampType, true) // Group/1
      :: StructField("status", ArrayType(StringType), true) // Practitioner/1
      :: StructField("status-simple", StringType, true) // Practitioner/1
      :: StructField("type", ArrayType(StringType), true) // Practitioner/1
      :: StructField("type-simple", StringType, true) // Practitioner/1
      :: StructField("title", StringType, true) // Practitioner/1
      :: StructField("cloud", ArrayType(StringType), true) // [one, two, three]
      :: StructField("random", IntegerType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRCompositionSolrSync = {
    val a = new FHIRCompositionSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(fact: Dataset[Row], concept: DataFrame, sectionList: List[String], noteCalc: DataFrame): DataFrame = {
    // add the sections to a list
    sectionList.foreach(f => {
      outputSchema = outputSchema.add(f, StringType, true, Metadata.empty)
    })

    val factDf = DFTool.applySchemaSoft(fact, compositionSchema)
    val conceptDf = DFTool.applySchemaSoft(concept, conceptFhirSchema)

    val note = getLastUpdatedDf("f" :: "v" :: "cs" :: Nil, factDf.alias("f")
      .filter(col("f.note_event_id").isNull)
      .join((conceptDf).alias("v"), col("f.note_class_source_concept_id") === col("v.concept_id"), "left")
      .join((conceptDf).alias("cs"), col("f.row_status_source_concept_id") === col("cs.concept_id"), "left")
      .withColumn("status", fhirTokenArray("cs"))
      .withColumn("status-simple", fhirTokenSimple("cs"))
      .withColumn("type", fhirTokenArray("v"))
      .withColumn("type-simple", fhirTokenSimple("v"))
      .withColumn("author", col("f.provider_id"))
      .withColumn("author-type", lit("Practitioner"))
      .withColumn("encounter", col("f.visit_occurrence_id"))
      .withColumn("encounter-type", lit("Encounter"))
      .withColumn("patient", col("f.person_id"))
      .withColumn("patient-type", lit("Patient"))
      .withColumn("random", expr("cast(rand()*100 as integer)"))
      .withColumnRenamed("note_id", "id")
      .withColumnRenamed("note_title", "title")
      .withColumnRenamed("note_datetime", "date"))
      .select("id", "title", "_lastUpdated", "status"
        , "status-simple", "type", "type-simple", "author", "encounter"
        , "patient", "author-type", "encounter-type", "patient-type"
        , "date", "random")

    val section = factDf.alias("s")
      .filter(col("s.note_event_id").isNotNull)
      .join((conceptDf).alias("v"), col("s.note_class_source_concept_id") === col("v.concept_id"), "left")
      .withColumn("key", concat(lit("section_"), col("v.concept_code"), lit("_txt")))
      .select("note_event_id", "key", "note_text")

    val sectionPivot = DFTool.simplePivot(section, col("note_event_id"), col("key"), "note_text", sectionList)
      .drop("note_text", "key", "note_text")

    var result = note.alias("r")
      .join(sectionPivot.alias("s"), col("s.note_event_id") === col("r.id"), "inner")
      .drop("note_event_id")

    result = result.as("r").join(noteCalc.selectExpr("note_id", "token_idf as cloud").as("c"), col("c.note_id") === col("r.id"), "left")

    result
  }
}
