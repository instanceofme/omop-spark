/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * This file is part of OMOP-SPARK.
 *
 * OMOP-SPARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OMOP-SPARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.types._

class MapperConceptSync(spark: SparkSession, options: Map[String, String], deltaPath: String, url: String) extends SolrSync(spark, options, deltaPath, url) {

  override def transform(concept_ids_str: Option[String] = None, join: Option[String] = None): this.type = {
    // get data from potgres
    // and validate it

    val concept_ids = strToLong(concept_ids_str)
    val concept = pgQuery(query = MapperConceptSync.extractConcept(concept_ids, join), partitionColumn = "concept_id")
    val project = pgQuery(query = MapperConceptSync.extractProject(), partitionColumn = "m_project_id")
    val avg = pgQuery(query = MapperConceptSync.extractAvg(concept_ids, join), partitionColumn = "concept_id")
    val freq = pgQuery(query = MapperConceptSync.extractFreq(concept_ids, join), partitionColumn = "concept_id")
    val synonyms = pgQuery(query = MapperConceptSync.extractSynonyms(concept_ids, join), partitionColumn = "concept_id")
    val relationship = pgQuery(query = MapperConceptSync.extractConceptRelationship(concept_ids, join), partitionColumn = "concept_id_1")

    this.dfTransformed = MapperConceptSync.transform(concept, project, avg, freq, synonyms, relationship)
    return this
  }

  def strToLong(concept_ids_str: Option[String]): Option[List[Long]] = {
    concept_ids_str match {
      case None => None
      case Some(x) => Option(x.split(",").map(s => s.toLong).toList)
    }
  }
}

object MapperConceptSync extends T {

  val RELATIONSHIP_EQUIVALENT = " 'maps to', 'mapped from' "
  val RELATIONSHIP_HIERARCHICAL = " 'is a' "

  def apply(spark: SparkSession, options: Map[String, String], deltaPath: String,
            url: String): MapperConceptSync = {
    val a = new MapperConceptSync(spark, options, deltaPath, url)
    a.initPg()
    return a
  }

  def transform(concept: Dataset[Row], project: Dataset[Row], stats_avg: Dataset[Row], stats_freq: Dataset[Row], concept_synonym: Dataset[Row], concept_relationship: Dataset[Row]): Dataset[Row] = {
    val spark = concept.toDF.sparkSession

    DFTool.applySchemaSoft(concept, conceptSchema).registerTempTable("concept")
    DFTool.applySchemaSoft(project, projectSchema).registerTempTable("project")
    DFTool.applySchemaSoft(stats_avg, statsAvgSchema).registerTempTable("stats_avg")
    DFTool.applySchemaSoft(stats_freq, statsFreqSchema).registerTempTable("stats_freq")
    DFTool.applySchemaSoft(concept_synonym, conceptSynonymSchema).registerTempTable("concept_synonym")
    DFTool.applySchemaSoft(concept_relationship, conceptRelationshipSchema).registerTempTable("concept_relationship")

    spark.sql(
      """
   SELECT concept_id                                                                                                                                                                                                                                                                                                                                                                                                              
   , collect_list(concept_synonym_name) as concept_synonym_name_en                                                                                                                                                                                                                                                                                                                                                                
   FROM concept_synonym                                                                                                                                                                                                                                                                                                                                                                                                           
   JOIN concept USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                                
   WHERE concept_synonym_name != concept_name                                                                                                                                                                                                                                                                                                                                                                                     
   AND concept_synonym.language_concept_id = 4180186 -- ENGLISH                                                                                                                                                                                                                                                                                                                                                                   
   GROUP BY concept_id                                                                                                                                                                                                                                                                                                                                                                                                            
""").registerTempTable("dfsynen")

    spark.sql(
      """
   SELECT concept_id                                                                                                                                                                                                                                                                                                                                                                                                              
   , collect_list(concept_synonym_name) as concept_synonym_name_fr                                                                                                                                                                                                                                                                                                                                                                
   FROM concept_synonym                                                                                                                                                                                                                                                                                                                                                                                                           
   JOIN concept USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                                
   WHERE concept_synonym_name != concept_name                                                                                                                                                                                                                                                                                                                                                                                     
   AND concept_synonym.language_concept_id = 4180190 -- FRENCH                                                                                                                                                                                                                                                                                                                                                                    
   GROUP BY concept_id                                                                                                                                                                                                                                                                                                                                                                                                            
""").registerTempTable("dfsynfr")

    spark.sql(
      """
   SELECT cr.concept_id_1                 as concept_id                                                                                                                                                                                                                                                                                                                                                                           
   ,collect_list(cpt3.concept_name) as standard_concept_mapped_name                                                                                                                                                                                                                                                                                                                                                               
   ,collect_list(cpt4.concept_name) as non_standard_concept_mapped_name                                                                                                                                                                                                                                                                                                                                                           
   ,collect_list(cpt5.concept_name) as concept_relation_name                                                                                                                                                                                                                                                                                                                                                                      
   FROM concept_relationship as cr                                                                                                                                                                                                                                                                                                                                                                                                
   LEFT JOIN concept as cpt3 on (cr.concept_id_2 = cpt3.concept_id AND lower(cr.relationship_id) IN ('maps to')                                                                                                                                                                                                                                                                                                            
     AND lower(cpt3.standard_concept) IS NOT DISTINCT FROM 's')                                                                                                                                                                                                                                                                                                                                                                   
   LEFT JOIN concept as cpt4 on (cr.concept_id_2 = cpt4.concept_id AND lower(cr.relationship_id) IN ('maps to')                                                                                                                                                                                                                                                                                                            
     AND lower(cpt4.standard_concept) IS DISTINCT FROM 's')                                                                                                                                                                                                                                                                                                                                                                       
   LEFT JOIN concept as cpt5 on (cr.concept_id_2 = cpt5.concept_id AND lower(cr.relationship_id) NOT IN ('maps to') )                                                                                                                                                                                                                                                                                                      
   GROUP BY cr.concept_id_1                                                                                                                                                                                                                                                                                                                                                                                                       
""").registerTempTable("mappedDF")

    spark.sql(
      f"""
   SELECT cr.concept_id_1                 as concept_id                                                                                                                                                                                                                                                                                                                                                                           
   ,collect_list(cpt.concept_name) as concept_mapped_name_en                                                                                                                                                                                                                                                                                                                                                                      
  FROM concept_relationship as cr                                                                                                                                                                                                                                                                                                                                                                                                 
   LEFT JOIN concept as cpt on (cr.concept_id_2 = cpt.concept_id AND lower(cr.relationship_id) IN ($RELATIONSHIP_EQUIVALENT))                                                                                                                                                                                                                                                                                                                  
  WHERE cpt.m_language_id IS NULL OR cpt.m_language_id = 'EN'                                                                                                                                                                                                                                                                                                                                                                     
  GROUP BY cr.concept_id_1                                                                                                                                                                                                                                                                                                                                                                                                        
""").registerTempTable("dfmapen")

    spark.sql(
      f"""
   SELECT                                                                                                                                                                                                                                                                                                                                                                                                                         
     cr.concept_id_1                 as concept_id                                                                                                                                                                                                                                                                                                                                                                                
   , collect_list(cpt.concept_name) as concept_mapped_name_fr                                                                                                                                                                                                                                                                                                                                                                     
  FROM concept_relationship as cr                                                                                                                                                                                                                                                                                                                                                                                                 
   LEFT JOIN concept as cpt on (cr.concept_id_2 = cpt.concept_id AND lower(cr.relationship_id) IN ($RELATIONSHIP_EQUIVALENT))                                                                                                                                                                                                                                                                                                                  
  WHERE cpt.m_language_id = 'FR'                                                                                                                                                                                                                                                                                                                                                                                                  
  GROUP BY cr.concept_id_1                                                                                                                                                                                                                                                                                                                                                                                                        
""").registerTempTable("dfmapfr")

    spark.sql(
      f"""
   SELECT                                                                                                                                                                                                                                                                                                                                                                                                                         
     cr.concept_id_1                 as concept_id                                                                                                                                                                                                                                                                                                                                                                                
   , collect_list(cpt.concept_name)  as concept_hierarchy_name_fr                                                                                                                                                                                                                                                                                                                                                                      
  FROM concept_relationship as cr                                                                                                                                                                                                                                                                                                                                                                                                 
   LEFT JOIN concept as cpt on (cr.concept_id_2 = cpt.concept_id AND lower(cr.relationship_id) IN ($RELATIONSHIP_HIERARCHICAL))                                                                                                                                                                                                                                                                                                                  
  WHERE cpt.m_language_id = 'FR'                                                                                                                                                                                                                                                                                                                                                                                                  
  GROUP BY cr.concept_id_1                                                                                                                                                                                                                                                                                                                                                                                                        
""").registerTempTable("dfhierafr")

    spark.sql(
      f"""
   SELECT                                                                                                                                                                                                                                                                                                                                                                                                                         
     cr.concept_id_1                 as concept_id                                                                                                                                                                                                                                                                                                                                                                                
   , collect_list(cpt.concept_name)  as concept_hierarchy_name_en                                                                                                                                                                                                                                                                                                                                                                     
  FROM concept_relationship as cr                                                                                                                                                                                                                                                                                                                                                                                                 
   LEFT JOIN concept as cpt on (cr.concept_id_2 = cpt.concept_id AND lower(cr.relationship_id) IN ($RELATIONSHIP_HIERARCHICAL))                                                                                                                                                                                                                                                                                                                  
  WHERE cpt.m_language_id = 'EN'                                                                                                                                                                                                                                                                                                                                                                                                  
  GROUP BY cr.concept_id_1                                                                                                                                                                                                                                                                                                                                                                                                        
""").registerTempTable("dfhieraen")

    // concepts already mapped to local concepts
    spark.sql(
      """
  SELECT concept_id_2 as concept_id                                                                                                                                                                                                                                                                                                                                                                                               
  , count(1) as local_map_number                                                                                                                                                                                                                                                                                                                                                                                                  
  FROM concept_relationship                                                                                                                                                                                                                                                                                                                                                                                                       
  WHERE concept_id_1 >= 2000000000                                                                                                                                                                                                                                                                                                                                                                                                
  group by concept_id_2                                                                                                                                                                                                                                                                                                                                                                                                           
""").registerTempTable("localMapDF")

    val outputSchema = StructType(
      StructField("id", StringType, false)
        :: StructField("concept_id", LongType, false)
        :: StructField("concept_name", StringType, false)
        :: StructField("concept_name_en", StringType, true)
        :: StructField("concept_name_fr", StringType, true)
        :: StructField("concept_name_fr_en", StringType, true)
        :: StructField("domain_id", StringType, false)
        :: StructField("vocabulary_id", StringType, false)
        :: StructField("concept_class_id", StringType, false)
        :: StructField("standard_concept", StringType, false)
        :: StructField("invalid_reason", StringType, false)
        :: StructField("concept_code", StringType, false)
        :: StructField("concept_synonym_name_en", ArrayType(StringType), true)
        :: StructField("concept_synonym_name_fr", ArrayType(StringType), true)
        :: StructField("concept_synonym_name_fr_en", ArrayType(StringType), true)
        :: StructField("concept_mapped_name_en", ArrayType(StringType), true)
        :: StructField("concept_mapped_name_fr", ArrayType(StringType), true)
        :: StructField("concept_hierarchy_name_en", ArrayType(StringType), true)
        :: StructField("concept_hierarchy_name_fr", ArrayType(StringType), true)
        :: StructField("concept_mapped_name_fr_en", ArrayType(StringType), true)
        :: StructField("m_language_id", StringType, false)
        :: StructField("frequency", DoubleType, true)
        :: StructField("is_mapped", StringType, false)
        :: StructField("local_map_number", LongType, false)
        :: StructField("value_avg", DoubleType, true)
        :: StructField("value_is_numeric", BooleanType, true)
        :: StructField("m_project_type_id", StringType, true)
        :: Nil)

    val projectUDF: UserDefinedFunction = udf((str: String, project: String) => {
      if (str.contains(" - ")) {
        str
      } else {
        f"$project - $str"
      }

    })

    spark.sqlContext.udf.register("projectUDF", projectUDF)

    val ret = spark.sql(
      """
   SELECT cast(concept_id as string) as id                                                                                                                                                                                                                                                                                                                                                                                                        
   , concept_id                                                                                                                                                                                                                                                                                                                                                                                                                   
   , COALESCE(concept_name, 'EMPTY')        as concept_name                                                                                                                                                                                                                                                                                                                                                                       
   , CASE WHEN m_language_id IS NULL OR m_language_id = 'EN' THEN COALESCE(concept_name, 'EMPTY') ELSE null END  as concept_name_en                                                                                                                                                                                                                                                                                                                       
   , CASE WHEN m_language_id = 'FR'  THEN COALESCE(concept_name, 'EMPTY') ELSE null END  as concept_name_fr                                                                                                                                                                                                                                                                                                                       
   , CASE WHEN m_language_id = 'FR'  THEN COALESCE(concept_name, 'EMPTY') ELSE null END  as concept_name_fr_en                                                                                                                                                                                                                                                                                                                    
   , projectUDF(COALESCE(domain_id, 'EMPTY'), m_project_type_id)           as domain_id                                                                                                                                                                                                                                                                                                                            
   , projectUDF(COALESCE(vocabulary_id, 'EMPTY'), m_project_type_id)       as vocabulary_id                                                                                                                                                                                                                                                                                                                        
   , projectUDF(COALESCE(concept_class_id, 'EMPTY'), m_project_type_id)    as concept_class_id                                                                                                                                                                                                                                                                                                                     
   , projectUDF(COALESCE(standard_concept, 'EMPTY'), m_project_type_id)    as standard_concept                                                                                                                                                                                                                                                                                                                     
   , projectUDF(COALESCE(invalid_reason, 'EMPTY'), m_project_type_id)      as invalid_reason                                                                                                                                                                                                                                                                                                                       
   , COALESCE(concept_code, 'EMPTY')        as concept_code                                                                                                                                                                                                                                                                                                                                                                       
   , concept_synonym_name_en                                                                                                                                                                                                                                                                                                                                                                                                 
   , concept_synonym_name_fr                                                                                                                                                                                                                                                                                                                                                                                                  
   , concept_synonym_name_fr as concept_synonym_name_fr_en                                                                                                                                                                                                                                                                                                                                                                        
   , concept_mapped_name_en                                                                                                                                                                                                                                                                                                                                                                                                      
   , concept_mapped_name_fr  
   , concept_hierarchy_name_en                                                                                                                                                                                                                                                                                                                                                                                                        
   , concept_hierarchy_name_fr                                                                                                                                                                                                                                                                                                                                                                                                        
   , concept_mapped_name_fr as concept_mapped_name_fr_en                                                                                                                                                                                                                                                                                                                                                                          
   , projectUDF(COALESCE(m_language_id, 'EN'), m_project_type_id)       as m_language_id                                                                                                                                                                                                                                                                                                                           
   , m_frequency_value   as frequency                                                                                                                                                                                                                                                                                                                                                                                             
   , projectUDF(CASE                                                                                                                                                                                                                                                                                                                                                                           
       WHEN standard_concept_mapped_name IS NOT NULL THEN 'S'                                                                                                                                                                                                                                                                                                                                                                     
       WHEN non_standard_concept_mapped_name IS NOT NULL THEN 'NS'                                                                                                                                                                                                                                                                                                                                                                
       WHEN concept_relation_name IS NOT NULL THEN 'R'                                                                                                                                                                                                                                                                                                                                                                            
       ELSE 'EMPTY'                                                                                                                                                                                                                                                                                                                                                                                                               
     END, m_project_type_id) as is_mapped                                                                                                                                                                                                                                                                                                                                                                                                             
   , COALESCE(local_map_number, 0) as local_map_number                                                                                                                                                                                                                                                                                                                                                                            
   , m_value_avg as value_avg                                                                                                                                                                                                                                                                                                                                                                                                     
   , m_value_avg is not null value_is_numeric                                                                                                                                                                                                                                                                                                                                                                                     
   , m_project_type_id as  m_project_type_id                                                                                                                                                                                                                                                                                                                                                                    
   FROM concept                                                                                                                                                                                                                                                                                                                                                                                                                   
   LEFT JOIN mappeddf USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                          
   LEFT JOIN dfsynen USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                           
   LEFT JOIN dfsynfr USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                           
   LEFT JOIN dfmapen USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                           
   LEFT JOIN dfhieraen USING (concept_id)         
   LEFT JOIN dfhierafr USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                     
   LEFT JOIN dfmapfr USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
   LEFT JOIN localMapDF USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                        
   LEFT JOIN stats_avg USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                         
   LEFT JOIN stats_freq USING (concept_id)                                                                                                                                                                                                                                                                                                                                                                                        
   LEFT JOIN project USING (m_project_id)                                                                                                                                                                                                                                                                                                                                                                                         
""")

    return DFTool.applySchemaSoft(ret, outputSchema)
  }

  /*
 *  concept
 */
  val conceptSchema = StructType(
    StructField("concept_id", LongType, false)
      :: StructField("concept_name", StringType, false)
      :: StructField("domain_id", StringType, false)
      :: StructField("vocabulary_id", StringType, true)
      :: StructField("concept_class_id", StringType, true)
      :: StructField("standard_concept", StringType, true)
      :: StructField("concept_code", StringType, true)
      :: StructField("m_language_id", StringType, true)
      :: StructField("m_project_id", IntegerType, true)
      :: StructField("invalid_reason", StringType, true)
      :: Nil)

  def extractConcept(concept_id: Option[List[Long]] = None, join: Option[String] = None): String = {
    var query =
      f"""
  select concept_id                                                                                                                                                                                              
  , concept_name                                                                                                                                                                                                 
  , domain_id                                                                                                                                                                                                    
  , vocabulary_id                                                                                                                                                                                                
  , concept_class_id                                                                                                                                                                                             
  , standard_concept                                                                                                                                                                                             
  , concept_code                                                                                                                                                                                                 
  , m_language_id    
  , m_project_id                                                                                                                                                                                                
  , invalid_reason                                                                                                                                                                                                                                                                                                                                                                                          
  from concept
  where true                                                                                                                                                                                                    
  """
    if (!concept_id.isEmpty)
      query += f" and concept_id IN (${expand(concept_id.get)})"
    if (!join.isEmpty)
      query += " AND concept_id " + join.get

    return query
  }

  /*
 *  project
 */
  val projectSchema = StructType(
    StructField("m_project_id", IntegerType, false)
      :: StructField("m_project_type_id", StringType, false)
      :: Nil)

  def extractProject(): String = {

    var query =
      f"""
  select m_project_id                                                                                                                                                                                            
  , m_project_type_id                                                                                                                                                                                            
  from mapper_project                                                                                                                                                                                            
  """
    return query
  }

  /*
 *  stats AVG
 */
  val statsAvgSchema = StructType(
    StructField("concept_id", LongType, false)
      :: StructField("m_value_avg", DoubleType, false)
      :: Nil)

  def extractAvg(concept_id: Option[List[Long]] = None, join: Option[String] = None): String = {
    var query =
      f"""
  select m_concept_id as concept_id                                                                                                                                                                              
  , m_value_as_number as m_value_avg                                                                                                                                                                             
  from mapper_statistic                                                                                                                                                                                          
  where m_statistic_type_id = 'AVG' 
  AND  m_value_as_number is not null                                                                                                                                                                              
  """
    if (!concept_id.isEmpty)
      query += f" and m_concept_id IN (${expand(concept_id.get)})"
    if (!join.isEmpty)
      query += " AND m_concept_id " + join.get
    return query
  }

  /*
 * stats freq
 */
  val statsFreqSchema = StructType(
    StructField("concept_id", LongType, false)
      :: StructField("m_frequency_value", DoubleType, false)
      :: Nil)

  def extractFreq(concept_id: Option[List[Long]] = None, join: Option[String] = None): String = {
    var query =
      f"""
  select m_concept_id as concept_id                                                                                                                                                                              
  , m_value_as_number as m_frequency_value                                                                                                                                                                       
  from mapper_statistic                                                                                                                                                                                          
  where m_statistic_type_id = 'FREQ'
  AND  m_value_as_number is not null                                                                                                                                                                           
  """
    if (!concept_id.isEmpty)
      query += f" and m_concept_id IN (${expand(concept_id.get)})"
    if (!join.isEmpty)
      query += " AND m_concept_id " + join.get
    return query
  }

  /*
   *  concept_synonym
   */
  val conceptSynonymSchema = StructType(
    StructField("concept_id", LongType, false)
      :: StructField("concept_synonym_name", StringType, false)
      :: StructField("language_concept_id", LongType, false)
      :: Nil)

  def extractSynonyms(concept_id: Option[List[Long]] = None, join: Option[String] = None): String = {

    var query =
      f"""
  select                                                                                                                                                                                                         
    concept_id                                                                                                                                                                                                   
  , concept_synonym_name                                                                                                                                                                                         
  , language_concept_id                                                                                                                                                                                          
  from concept_synonym
  where true                                                                                                            
  """
    if (!concept_id.isEmpty)
      query += f" and concept_id IN (${expand(concept_id.get)})"
    if (!join.isEmpty)
      query += " AND concept_id " + join.get
    return query
  }

  val conceptRelationshipSchema = StructType(
    StructField("concept_id_1", LongType, false)
      :: StructField("concept_id_2", LongType, false)
      :: StructField("relationship_id", StringType, false)
      :: Nil)

  def extractConceptRelationship(concept_id: Option[List[Long]] = None, join: Option[String] = None): String = {
    var query =
      f"""
  select                                                                                                                                                                                                         
    concept_id_1                                                                                                                                                                                                 
  , concept_id_2                                                                                                                                                                                                 
  , relationship_id                                                                                                                                                                                              
  from concept_relationship                                                                                                                                                                                      
  where concept_id_1 != concept_id_2                                                                                                                                                                             
  AND m_modif_end_datetime IS NULL  
  AND invalid_reason IS DISTINCT FROM 'D'                                                                                                                                                                              
  """
    if (!concept_id.isEmpty)
      query += f" AND concept_id_1 IN (${expand(concept_id.get)})"
    if (!join.isEmpty)
      query += " AND concept_id_1 " + join.get
    return query
  }
}
