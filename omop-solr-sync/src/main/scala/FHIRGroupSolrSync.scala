/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * This file is part of OMOP-SPARK.
 *
 * OMOP-SPARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OMOP-SPARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._


class FHIRGroupSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "groupAphp"


  override def transformDelta(concept_fhir: DataFrame): this.type = {


    logger.warn("Generating %s from OMOP".format(_collection))
    val cohortDefinition = spark.read.format("delta").load(deltaPath + "cohort_definition")
    this.dfTransformed = FHIRGroupSolrSync.transform(cohortDefinition, concept_fhir)
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRGroupSolrSync.outputSchema)
    writeCollection()


    this
  }

}

object FHIRGroupSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, false)
      :: StructField("created-date", TimestampType, false)
      :: StructField("managing-entity", LongType, true) // Group/1
      :: StructField("managing-entity-type", StringType, true) // Group/1
      :: StructField("type", ArrayType(StringType), true) // Practitioner/1
      :: StructField("type-simple", StringType, true) // Practitioner/1
      :: StructField("_text_", StringType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRGroupSolrSync = {
    val a = new FHIRGroupSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(fact: Dataset[Row], concept: Dataset[Row]): Dataset[Row] = {

    val factDf = DFTool.applySchemaSoft(fact, cohortDefinitionSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concept, conceptFhirSchema)

    val ret = getLastUpdatedDf("f" :: "v" :: Nil, factDf.alias("f")
      .join(conceptFhirDf.alias("v"), col("f.subject_source_concept_id") === col("v.concept_id"), "left")
      .withColumn("type", fhirTokenArray("v"))
      .withColumn("type-simple", fhirTokenSimple("v"))
      .withColumn("created-date", col("f.cohort_initiation_datetime"))
      .withColumn("managing-entity", when(
        col("owner_domain_id") === lit("Provider"), col("owner_entity_id"))
        .otherwise(when(col("owner_domain_id") === lit("Care_site"), col("owner_entity_id"))
          .otherwise(col("owner_entity_id"))))
      .withColumn("managing-entity-type", when(
        col("owner_domain_id") === lit("Provider"), lit("Practitioner"))
        .otherwise(when(col("owner_domain_id") === lit("Care_site"), lit("Organization"))
          .otherwise(lit("Error")))))
      .withColumn("_text_", concat(col("cohort_definition_name"), lit("\n"), fhirNarrative("v")))
      .select("cohort_definition_id", "type",
        "type-simple", "managing-entity-type", "managing-entity",
        "_lastUpdated", "created-date", "_text_")

    val groupColumns = Map("cohort_definition_id" -> "id")

    return DFTool.applySchemaSoft(DFTool.dfRenameColumn(ret, groupColumns), outputSchema)
  }
}
