/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

class FHIRValueSetSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "valueSetAphp"

  override def transformDelta(conceptFhir: DataFrame): this.type = {
    logger.warn("Generating %s from OMOP".format(_collection))
    val concept = spark.read.format("delta").load(deltaPath + "concept")
    val concept_relationship = spark.read.format("delta").load(deltaPath + "concept_relationship")
    val vocabulary = spark.read.format("delta").load(deltaPath + "vocabulary")
    this.dfTransformed = FHIRValueSetSolrSync.transform(concept, concept_relationship, vocabulary)
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed,
      FHIRValueSetSolrSync.outputSchema)

    writeCollection()

    this
  }

}


object FHIRValueSetSolrSync
  extends T {

  val outputSchema = types.StructType(
    StructField("id", StringType, false)
      :: StructField("concept_id", IntegerType, false)
      :: StructField("vocabulary_reference", StringType, false)
      :: StructField("concept_code", StringType, false)
      :: StructField("concept_name", StringType, false)
      :: StructField("child", ArrayType(IntegerType), false)
      :: StructField("parent", ArrayType(IntegerType), false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRValueSetSolrSync = {
    val a = new FHIRValueSetSolrSync(spark, options, url, deltaPath, delay)
    return a
  }

  def transform(concepts: DataFrame, concept_rel: DataFrame, voc: DataFrame): DataFrame = {

    import concepts.sparkSession.implicits._

    val rel_child = concept_rel.filter(col("relationship_id").isin("Subsumes", "Contains")).as("rc")
    val rel_parent = concept_rel.filter(col("relationship_id").isin("Is a", "Contained in")).as("rp")
    val conc = concepts.join(voc.drop("change_datetime"), "vocabulary_id")

    val child = conc.as("c1")
      .join(rel_child.as("fr"), col("c1.concept_id") === col("fr.concept_id_1"), "left")
      .join(concepts.as("c2"), col("fr.concept_id_2") === col("c2.concept_id"), "left")
      .groupBy(col("c1.concept_id"), col("c1.concept_code"), col("c1.concept_name"), col("c1.vocabulary_reference"))
      .agg(array_distinct(collect_list(col("c2.concept_id"))).as("child"))
      .select('concept_id, 'concept_code, 'concept_name, 'vocabulary_reference, 'child)

    val parent = conc.as("c1")
      .join(rel_parent.as("fp"), col("c1.concept_id") === col("fp.concept_id_1"), "left")
      .join(concepts.as("c2"), col("fp.concept_id_2") === col("c2.concept_id"), "left")
      .groupBy(col("c1.concept_id"))
      .agg(array_distinct(collect_list(col("c2.concept_id"))).as("parent"))
      .select('concept_id, 'parent)

    val res = child.join(parent, "concept_id")
      .withColumn("id", 'concept_id.cast(StringType))
      .withColumn("_lastUpdated", current_timestamp())

    DFTool.applySchemaSoft(res, outputSchema)
  }

}
