/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

/*

<field name="name" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="partof" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="type" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="active" type="text_general" indexed="true" stored="false" multiValued="true" />
<field name="identifier" type="text_general" indexed="true" stored="false" multiValued="true" />

 *
 */

class FHIRPractitionerRoleSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "practitionerRoleAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {


    logger.warn("Generating %s from OMOP".format(_collection))
    val care_site_history = spark.read.format("delta").load(deltaPath + "care_site_history")
      .filter(col("domain_id") === lit("Provider"))
    this.dfTransformed = FHIRPractitionerRoleSolrSync.transform(care_site_history)
    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRPractitionerRoleSolrSync.outputSchema)
    writeCollection()

    this
  }

}

object FHIRPractitionerRoleSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("practitioner", LongType, true) // Group/1
      :: StructField("practitioner-type", StringType, true) // Group/1
      :: StructField("organization", LongType, true) // Group/1
      :: StructField("organization-type", StringType, true) // Group/1
      :: StructField("active", BooleanType, true) // Group/1
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int] = None): FHIRPractitionerRoleSolrSync = {
    val a = new FHIRPractitionerRoleSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(fact: Dataset[Row]): Dataset[Row] = {

    val factDf = DFTool.applySchemaSoft(fact, careSiteHistorySchema)

    val ret = factDf.alias("f")
      .withColumnRenamed("change_datetime", "_lastUpdated")
      .withColumn("practitioner", col("f.entity_id"))
      .withColumn("practitioner-type", lit("Practitioner"))
      .withColumn("organization", col("f.care_site_id"))
      .withColumn("organization-type", lit("Organization"))
      .withColumn("active", when(col("f.end_date").isNull, lit(true)).otherwise(lit(false)))
      .select("care_site_history_id", "organization",
        "practitioner", "organization-type",
        "practitioner-type", "_lastUpdated", "active")

    val groupColumns = Map(
      "care_site_history_id" -> "id")

    return DFTool.applySchemaSoft(DFTool.dfRenameColumn(ret, groupColumns), outputSchema)
  }
}
