/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}


class FHIRProcedureSolrSync(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String, delay: Option[Int])
  extends SolrSync(spark, options, url, deltaPath, delay) with T {

  _collection = "procedureAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {


    logger.warn("Generating %s from OMOP".format(_collection))
    var procedure = spark.read.format("delta").load(deltaPath + "procedure_occurrence")

    this.dfTransformed = FHIRProcedureSolrSync.transform(procedure.repartition(_partitionNumLarge), concept_fhir)

    this.dfTransformed = DFTool.applySchemaSoft(this.dfTransformed, FHIRProcedureSolrSync.outputSchema)
    writeCollection(joinStr = "encounterAphp")

    this
  }

}

object FHIRProcedureSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("date", TimestampType, true)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("encounter", LongType, true)
      :: StructField("encounter-type", StringType, true)
      :: StructField("category", ArrayType(StringType), false)
      :: StructField("category-simple", StringType, false)
      :: StructField("code", ArrayType(StringType), false)
      :: StructField("code-simple", StringType, false)
      :: StructField("status", ArrayType(StringType), true)
      :: StructField("status-simple", StringType, true)
      :: StructField("_text_", StringType, true)
      :: Nil)

  def apply(spark: SparkSession, options: Map[String, String], url: String, deltaPath: String,
            delay: Option[Int] = None): FHIRProcedureSolrSync = {
    val a = new FHIRProcedureSolrSync(spark, options, url, deltaPath, delay)
    a.initPg()
    return a
  }

  def transform(procedure: Dataset[Row], concept: DataFrame): DataFrame = {

    val procedureDf = DFTool.applySchemaSoft(procedure, procedureSchema)
    val conceptDf = DFTool.applySchemaSoft(concept, conceptFhirSchema)

    val ret = getLastUpdatedDf("f" :: "c" :: "v" :: "cs" :: Nil, procedureDf.alias("f")
      .join((conceptDf).alias("c"), col("f.procedure_source_concept_id") === col("c.concept_id"), "left")
      .join((conceptDf).alias("v"), col("f.procedure_type_source_concept_id") === col("v.concept_id"), "left")
      .join((conceptDf).alias("cs"), col("f.row_status_source_concept_id") === col("cs.concept_id"), "left")
      .withColumn("date", col("procedure_datetime"))
      .withColumn("patient", col("f.person_id"))
      .withColumn("patient-type", lit("Patient"))
      .withColumn("encounter", col("visit_occurrence_id"))
      .withColumn("_text_", fhirNarrative("c", "v", "cs"))
      .withColumn("encounter-type", lit("Encounter")))
      .withColumn("category", fhirTokenArray("v"))
      .withColumn("code", fhirTokenArray("c"))
      .withColumn("status", fhirTokenArray("cs"))
      .withColumn("category-simple", fhirTokenSimple("v"))
      .withColumn("code-simple", fhirTokenSimple("c"))
      .withColumn("status-simple", fhirTokenSimple("cs"))
      .withColumnRenamed("procedure_occurrence_id", "id")
      .select("id",
        "patient", "patient-type",
        "encounter", "encounter-type",
        "_lastUpdated", "_text_", "date"
        , "category", "code", "status"
        , "category-simple", "code-simple", "status-simple")


    return DFTool.applySchemaSoft(ret, outputSchema)

  }
}
