/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.solr

import com.typesafe.scalalogging.LazyLogging
import io.frama.parisni.spark.dataframe.DFTool
import io.frama.parisni.spark.postgres.PGTool
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

abstract class SolrSync(spark: SparkSession,
                        optionsSolr: Map[String, String],
                        urlPostgres: String,
                        deltaPath: String,
                        delay: Option[Int] = None) extends T with LazyLogging with OmopTransform {

  protected var dfTransformed: Dataset[Row] = null
  protected var _pg: PGTool = null
  protected var _job_id: List[Int] = null
  protected var _collection: String = null
  protected var _partitionNumSmall: Int = 200
  protected var _partitionNumMedium: Int = 1000
  protected var _partitionNumLarge: Int = 5000

  def getJobId: (Int => List[Long]) = job_type_concept_id => {
    val query =
      """
      update job set status_id = 'PENDING'
      where job_type_id = %d
      and status_id = 'TODO'
      returning job_id
      """.format(job_type_concept_id)
    val jobIds = _pg.sqlExecWithResult(query)
    jobIds.select(col("job_id")).collect.map(row => row.getAs[Long](0)).toList
  }

  def finishJobId(listLong: Option[List[Long]]): Unit = {
    val query =
      """
      update job set status_id = 'FINISHED'
      where job_id IN ( %s )
      """.format(listLong.get.mkString(","))
    _pg.sqlExec(query)
  }

  def errorJobId(listLong: Option[List[Long]], errorMessage: String): Unit = {
    val query =
      """
      update job set status_concept_id = 'ERROR', error_msg = '%s'
      where job_id IN ( %s )
      """.format(listLong.get.mkString(","), errorMessage)
    _pg.sqlExec(query)
  }

  def generateJoin: ((List[Long] => Option[String])) = listLong => listLong match {
    case Nil => None
    case _ => Some("  IN ( SELECT id_to_process FROM job_process WHERE job_id IN (%s) )"
      .format(listLong.mkString(",")))
  }

  def transformJoin(job_type_concept_id: Int): this.type = {
    val listLong = getJobId(job_type_concept_id)
    try {
      transform(join = generateJoin(listLong))
      finishJobId(Some(listLong))
    } catch {
      case e: Exception => {
        logger.error(e.getMessage)
        errorJobId(Some(listLong), e.getMessage)
      }
    }
    this
  }

  def transform(concept_ids_str: Option[String] = None, join: Option[String] = None): this.type = {
    this
  }

  def transformDelta(conceptFhir: DataFrame): this.type = {
    this
  }

  def writeCollection(partitionNum: Option[Int] = Some(200), joinStr: String = "") = {

    this.dfTransformed = joinStr match {
      case "encounterAphp" => {
        val joinDf = spark.read.format("parquet").load("%s/%s".format(this.deltaPath, joinStr))
        addFeaturesEncounter(spark, this.dfTransformed, joinDf)
      }
      case "patientAphp" => {
        val joinDf = spark.read.format("parquet").load("%s/%s".format(this.deltaPath, joinStr))
        addFeaturesPatient(spark, this.dfTransformed, joinDf)
      }
      case _ => this.dfTransformed
    }
    // rewriting the whole table is more stable and also will simplify the source code
    DFTool.dfAddHash(this.dfTransformed.repartition(partitionNum.get, col("id")))
      .write.mode(SaveMode.Overwrite).format("parquet").save(s"${deltaPath}/${_collection}")

    //OmopSync.scd1(DFTool.dfAddHash(filterDelay(this.dfTransformed).repartition(partitionNum.get, col("id"))), _collection, "id", deltaPath)
  }

  def filterDelay(df: DataFrame) = {
    var qt = " fully"
    val tmp = if (delay.isDefined) {
      qt = " with %d mimutes".format(delay.get)
      df.filter(
        col("_lastUpdated").isNull ||
          col("_lastUpdated").>=(current_timestamp().minus(expr("INTERVAL %s minutes".format(delay.get)))))
    }
    else df
    logger.debug("Loading data%s with %d rows".format(qt, tmp.count()))
    tmp
  }

  def loadCollection(deltaPath: String, cohortDf: Option[DataFrame] = None) = {

    logger.warn("Loading %s from disk".format(_collection))
    // if the fhir tables exists then then only inner join are applied (cohort refresh)
    if (cohortDf.isDefined) {
      logger.warn("Cohort is defined".format(_collection))
      this.dfTransformed = getLastUpdatedDf("f" :: "cohort" :: Nil,
        spark.read.format("parquet").load(deltaPath + _collection)
          .withColumnRenamed("_lastUpdated", "change_datetime").as("f")
          .join(cohortDf.get.as("cohort"),
            col("f.%s".format("patient")) === col("cohort.person_id"),
            "inner"))
        .drop(col("cohort.person_id"))
    } else this.dfTransformed = spark.read.format("parquet").load(deltaPath + _collection).as("f")

  }


  /**
   *
   * @param numPartitions
   * @param cohortDf in case None, then no join with it will be made. The join is inner because either all patients are in the cohort either only a subset
   */
  def sync(numPartitions: Option[Int] = Some(500),
           cohortDf: Option[DataFrame] = None): Unit = {

    loadCollection(deltaPath, cohortDf)

    filterDelay(dfTransformed)
      .drop("hash", "change_datetime")
      .repartition(numPartitions.get, col("id"))
      .coalesce(4) // performance improvement for solr load
      .write
      .format("solr")
      .options(optionsSolr)
      .option("collection", _collection)
      .mode(org.apache.spark.sql.SaveMode.Overwrite)
      .save
  }

  def initPg(): SolrSync = {
    _pg = PGTool(spark, urlPostgres, "spark-postgres")
    return this
  }

  def pgQuery(query: String, partitionColumn: String): Dataset[Row] = {
    return _pg.inputBulk(query = query, isMultiline = Some(true), numPartitions = Some(4), splitFactor = Some(1), partitionColumn = partitionColumn)
  }

}

trait T {

  val conceptFhirSchema = StructType(
    StructField("concept_id", IntegerType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("vocabulary_reference", StringType)
      :: StructField("concept_code", StringType)
      :: StructField("concept_name", StringType)
      :: StructField("fhir_vocabulary_reference", StringType)
      :: StructField("fhir_concept_code", StringType)
      :: Nil)

  val procedureSchema = StructType(
    StructField("procedure_occurrence_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("person_id", LongType, false)
      :: StructField("visit_occurrence_id", LongType, true)
      :: StructField("procedure_source_concept_id", IntegerType, true)
      :: StructField("procedure_type_source_concept_id", IntegerType, true)
      :: StructField("row_status_source_concept_id", IntegerType, true)
      :: StructField("procedure_datetime", TimestampType)
      :: Nil)

  val conditionSchema = StructType(
    StructField("condition_occurrence_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("person_id", LongType, false)
      :: StructField("visit_occurrence_id", LongType, true)
      :: StructField("condition_source_concept_id", IntegerType, true)
      :: StructField("condition_type_source_concept_id", IntegerType, true)
      :: StructField("condition_status_source_concept_id", IntegerType, true)
      :: StructField("row_status_source_concept_id", IntegerType, true)
      :: StructField("condition_start_datetime", TimestampType)
      :: Nil)

  val observationPersonSchema = StructType(
    StructField("person_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("observation_source_concept_id", IntegerType)
      :: StructField("value_as_string", StringType)
      :: Nil)

  val observationVisitSchema = StructType(
    StructField("visit_occurrence_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("observation_source_concept_id", IntegerType)
      :: StructField("value_as_string", StringType)
      :: Nil)

  val personSchema = StructType(
    StructField("person_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("gender_source_concept_id", IntegerType)
      :: StructField("birth_datetime", TimestampType)
      :: StructField("death_datetime", TimestampType)
      :: StructField("row_status_source_concept_id", IntegerType)
      :: Nil)

  val visitOccurrenceSchema = StructType(
    StructField("visit_occurrence_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("visit_occurrence_source_value", StringType)
      :: StructField("visit_start_datetime", TimestampType)
      :: StructField("visit_end_datetime", TimestampType)
      :: StructField("visit_source_concept_id", LongType)
      :: StructField("visit_type_source_concept_id", IntegerType)
      :: StructField("row_status_source_concept_id", IntegerType)
      :: StructField("person_id", LongType)
      :: StructField("care_site_id", LongType)
      :: Nil)

  val visitDetailSchema = StructType(
    StructField("visit_detail_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("visit_detail_parent_id", LongType)
      :: StructField("visit_occurrence_id", LongType, false)
      :: StructField("visit_detail_source_concept_id", IntegerType)
      :: StructField("visit_detail_type_source_concept_id", IntegerType)
      :: StructField("visit_detail_start_datetime", TimestampType)
      :: StructField("visit_detail_end_datetime", TimestampType)
      :: StructField("person_id", LongType)
      :: StructField("row_status_source_concept_id", IntegerType)
      :: StructField("care_site_id", LongType)
      :: Nil)

  val documentReferenceSchema = StructType(
    StructField("note_id", LongType, false)
      :: StructField("visit_occurrence_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("note_datetime", TimestampType)
      :: StructField("note_type_source_concept_id", IntegerType)
      :: StructField("note_class_source_concept_id", IntegerType)
      :: StructField("person_id", LongType)
      :: StructField("note_title", StringType)
      :: StructField("row_status_source_concept_id", IntegerType)
      :: Nil)

  val careSiteSchema = StructType(
    StructField("care_site_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("care_site_source_value", StringType)
      :: StructField("care_site_name", StringType)
      :: StructField("care_site_short_name", StringType)
      :: StructField("place_of_service_source_concept_id", IntegerType)
      :: StructField("valid_end_date", DateType)
      :: Nil)

  val factRelationshipSchema = StructType(
    StructField("fact_id_1", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("fact_id_2", LongType, false)
      :: StructField("relationship_concept_id", IntegerType, false)
      :: Nil)

  val providerSchema = StructType(
    StructField("provider_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("lastname", StringType, false)
      :: StructField("firstname", StringType, false)
      :: StructField("gender_source_concept_id", IntegerType, false)
      :: StructField("provider_source_value", StringType, false)
      :: StructField("valid_end_datetime", TimestampType, false)
      :: Nil)

  val cohortDefinitionSchema = StructType(
    StructField("cohort_definition_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("cohort_initiation_datetime", TimestampType)
      :: StructField("cohort_definition_name", StringType, true)
      :: StructField("owner_entity_id", LongType, false)
      :: StructField("owner_domain_id", StringType, false)
      :: StructField("subject_source_concept_id", IntegerType, false)
      :: Nil)

  val cohortListSchema = StructType(
    StructField("subject_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("_list", ArrayType(LongType))
      :: Nil)

  val careSiteHistorySchema = StructType(
    StructField("care_site_history_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("entity_id", LongType, true)
      :: StructField("care_site_id", LongType, true)
      :: StructField("end_date", TimestampType)
      :: Nil)

  val compositionSchema = StructType(
    StructField("note_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("visit_occurrence_id", LongType)
      :: StructField("note_event_id", LongType)
      :: StructField("note_datetime", TimestampType)
      :: StructField("note_class_source_concept_id", IntegerType)
      :: StructField("row_status_source_concept_id", IntegerType)
      :: StructField("person_id", LongType)
      :: StructField("provider_id", LongType)
      :: StructField("note_title", StringType)
      :: StructField("note_text", StringType)
      :: Nil)

  val costSchema = StructType(
    StructField("cost_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("person_id", LongType)
      :: StructField("cost_event_id", LongType)
      :: StructField("cost_event_field_concept_id", IntegerType)
      :: StructField("incurred_datetime", TimestampType)
      :: StructField("drg_source_concept_id", IntegerType)
      :: StructField("row_status_source_concept_id", IntegerType)
      :: Nil)

  val cohortSchema = StructType(
    StructField("subject_id", LongType, false)
      :: StructField("change_datetime", TimestampType)
      :: StructField("cohort_definition_id", LongType, true)
      :: StructField("cohort_id", LongType, true)
      :: Nil)

  val personSecuSchema = StructType(
    StructField("person_id", LongType, false)
      :: StructField("agg_care_site_id", ArrayType(LongType), true)
      :: Nil)

  def expand(lst: List[Long]): String = {
    lst.mkString(",")
  }

  def generateSelect(schema: StructType, table: String, where: String = ""): String = {
    val columns = schema.fieldNames.mkString(", ")
    f"""
      select 
      $columns
      from
      $table
      where true
      $where
      """
  }

  def generateCaseWhen(column: String, rep: Map[String, List[Int]]): String = {
    return "CASE " + rep.map(x => f"WHEN $column  IN (${x._2.mkString(",")}) then '${x._1}' ").mkString(" ") + "END"
  }

  def generateFilter(column: String, rep: Map[String, List[Int]]): String = {
    // observation_concept_id IN (4086933, 4086449, 4314148, 4245003)
    return f"$column  IN (" + rep.map(x => f" ${x._2.mkString(",")} ").mkString(" , ") + ")"
  }

  def fhirReference(resourceName: String, colname: Column): Column = {
    concat(lit("%s/".format(resourceName)), colname)
  }

  def fhirReference(resourceName: String, colname: String): Column = {
    fhirReference(resourceName, col(colname))
  }

  def fhirTokenSimple(tableAlias: String): Column = {
    col("%s.concept_code".format(tableAlias))
  }

  def fhirTokenArray(tableAlias: String): Column = {
    array(
      concat(coalesce(col("%s.vocabulary_reference".format(tableAlias)), lit("")), lit("|"), col("%s.concept_code".format(tableAlias))),
      concat(coalesce(col("%s.fhir_vocabulary_reference".format(tableAlias)), lit("")), lit("|"), col("%s.fhir_concept_code".format(tableAlias))))
  }

  def fhirTokenWithValueArray(tableAlias: String, value: String): Column = {
    val sep = lit("|")
    array(
      concat(coalesce(col("%s.vocabulary_reference".format(tableAlias)), lit("")), sep, col("%s.concept_code".format(tableAlias)), sep, col(value)),
      concat(coalesce(col("%s.fhir_vocabulary_reference".format(tableAlias)), lit("")), sep, col("%s.fhir_concept_code".format(tableAlias)), sep, col(value))
    )
  }

  def getLastUpdated = (l: List[String], c: String) => {
    greatest(l.map(x => col("%s.%s".format(x, c))): _*)
  }

  def getLastUpdatedDf = (l: List[String], df: DataFrame) => {
    df.withColumn("_lastUpdated", getLastUpdated(l, "change_datetime"))
  }

  def addColumnsViaFold(df: DataFrame, columns: List[String]): DataFrame = {
    import df.sparkSession.implicits._
    columns.foldLeft(df)((acc, col) => {
      acc.withColumn(col, acc("incipit").as[String].contains(col))
    })
  }

  def fhirReferenceMulti(typ: String, id: String, name: String) = {
    ((0 until 2).map(i => array(col(id), lit(typ))(i).alias(if (i == 0) {
      name
    } else {
      "%s-type".format(name)
    })))
  }

  def fhirNarrative(tables: String*): Column = {
    fhirNarrativeCol(tables.map(x => fhirCodeSystem(col("%s.concept_code".format(x)), col("%s.concept_name".format(x)))): _*)
  }

  def fhirNarrativeCol(cols: Column*): Column = {
    concat(cols.map(x => concat(coalesce(x, lit("")), lit("\n"))).toList: _*)
  }

  def fhirCodeSystem(cols: Column*): Column = {
    concat(cols.map(x => concat(coalesce(x, lit("")), lit(":"))).toList: _*)
  }

  def addDenormalizedTable(df: DataFrame, colSource: Column, colJoined: Column) = {

  }


  def fhirFacet(tableAlias: String): Column = {
    coalesce(
      col("%s.fhir_concept_code".format(tableAlias)),
      col("%s.concept_code".format(tableAlias))
    )
  }

}
