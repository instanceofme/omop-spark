/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.omop.section

import com.databricks.spark.corenlp.CoreNlpFunctions.{pos, ssplit, tokenize}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{coalesce, explode, lit}


object CoreNlpFrench extends App with LazyLogging {

  val indb = args(0)
  val input_table = args(1) // note_id | note_text
  val outdb = args(2)
  val output_table = args(3) // note_id | sentence | words | postag


  logger.warn("Source: %s.%s\nTarget: %s.%s".format(indb, input_table, outdb, output_table))

  val spark = SparkSession
    .builder()
    .appName("OMOP CoreNlp Extract")
    .enableHiveSupport()
    .getOrCreate()

  import spark.implicits._

  val note_input = spark.read.table("%s.%s".format(indb, input_table))

  val note_output = note_input
    .select('note_id,
      explode(ssplit(coalesce('note_text, lit("")))).as('sentence))
    .select('note_id,
      'sentence,
      tokenize('sentence).as('words),
      pos('sentence).as('postag))

  note_output.write.format("orc")
    .mode(org.apache.spark.sql.SaveMode.Overwrite)
    .saveAsTable("%s.%s".format(outdb, output_table))

  // save the resulting section table


}

