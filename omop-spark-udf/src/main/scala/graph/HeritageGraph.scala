/**
 *     This file is part of SPARK-OMOP.
 *
 *     SPARK-OMOP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     SPARK-OMOP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.omop.graph

import org.apache.spark.graphx._
import org.apache.spark.sql.DataFrame

// The code is based on Graphx Pregel API
  class HeritageGraph extends Serializable {

    //mutate the value of the vertices
    def setMsg(vertexId: VertexId, value: (Long, Int, List[Long], Int, Long, Int), message: (Long, Int, List[Long], Int, Int)): (Long, Int, List[Long], Int, Long, Int) = {
      if (message._2 < 1) { //superstep 0 - initialize
        (value._1, value._2 + 1, value._3, value._4, value._5, value._6)
      } else if (message._4 == 1) { // set isCyclic
        (value._1, value._2, value._3, message._4, value._5, value._6)
      } else if (message._5 == 0) { // set isleaf
        (value._1, value._2, value._3, value._4, value._5, message._5)
      } else { // set new values
        (message._1, value._2 + 1, value._5 :: message._3, value._4, value._5, value._6)
      }
    }

    // send the value to vertices
    def sendMsg(triplet: EdgeTriplet[(Long, Int, List[Long], Int, Long, Int), _]): Iterator[(VertexId, (Long, Int, List[Long], Int, Int))] = {
      val sourceVertex = triplet.srcAttr
      val destinationVertex = triplet.dstAttr
      // check for icyclic
      if (sourceVertex._1 == triplet.dstId || sourceVertex._1 == destinationVertex._1)
        if (destinationVertex._5 == 0) { //set iscyclic
          Iterator((triplet.dstId, (sourceVertex._1, sourceVertex._2, sourceVertex._3, 1, sourceVertex._6)))
        } else {
          Iterator.empty
        }
      else {
        if (sourceVertex._6 == 1) //is NOT leaf
        {
          Iterator((triplet.srcId, (sourceVertex._1, sourceVertex._2, sourceVertex._3, 0, 0)))
        }
        else { // set new values
          Iterator((triplet.dstId, (sourceVertex._1, sourceVertex._2, sourceVertex._3, 0, 1)))
        }
      }
    }


    // receive the values from all connected vertices
    def mergeMsg(msg1: (Long, Int, List[Long], Int, Int), msg2: (Long, Int, List[Long], Int, Int)): (Long, Int, List[Long], Int, Int) = {
      // dummy logic not applicable to the data in this usecase
      msg2
    }

    //setup & call the pregel api
    def calcTopLevelHierarcy(vertexDF: DataFrame, edgeDF: DataFrame, maxDepth:Int = Int.MaxValue): DataFrame = {

      // create the vertex RDD
      // primary key, root, path
      val verticesRDD = {
        vertexDF
          .rdd
          .map {
            x => (x.get(0), x.get(1))
          }
          .map {
            x => (x._1.asInstanceOf[Long], (x._1.asInstanceOf[Long]))
          }
      }

      // create the edge RDD
      // top down relationship
      val EdgesRDD = {
        edgeDF.rdd.map {
          x => (x.get(0), x.get(1))
        }
          .map {
            x => Edge(x._1.asInstanceOf[Long], x._2.asInstanceOf[Long], "topdown")
          }
      }

      // create graph
      @transient
      val graph = Graph(verticesRDD, EdgesRDD).cache()

      // initialize id,level,root,path,iscyclic, isleaf
      @transient
      val initialMsg = (0L, 0, List(0L), 0, 1)

      // add more dummy attributes to the vertices - id, level, root, path, isCyclic, existing value of current vertex to build path, isleaf, pk
      @transient
      val initialGraph = graph.mapVertices((id, v) => (id, 0, List(id), 0, id, 1))

      @transient
      val hierarchyRDD = initialGraph.pregel(initialMsg,
        maxDepth,
        EdgeDirection.Out)(
        setMsg,
        sendMsg,
        mergeMsg)

      // build the path from the list
      @transient
      val hierarchyOutRDD = hierarchyRDD.vertices.map {
        case (id, v) => (id.asInstanceOf[Long], (v._2, v._1, v._3, v._4, v._6))
      }

      import edgeDF.sparkSession.implicits._
      hierarchyOutRDD
        .map { case (pk, (level, root, path, iscyclic, isleaf)) => (pk, level, root, path, iscyclic, isleaf) }
        .toDF("id", "level", "root", "path", "iscyclic", "isleaf")

    }
  }

