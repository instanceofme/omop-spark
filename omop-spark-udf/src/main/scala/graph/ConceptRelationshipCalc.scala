/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.omop.graph

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 *
 * This code infers missing edges
 * from the concept_relationship table
 *
 */

object ConceptRelationshipCalc extends App  {

  def removeExistingCandidate(candidate: DataFrame, existing: DataFrame): DataFrame = {
    candidate.except(existing.selectExpr("concept_id_1", "concept_id_2"))
  }

  def generateCandidate(data: DataFrame): DataFrame = {

    data.as("g")
      .join(data.as("f"), col("g.concept_id_1") === col("f.concept_id_1"))
      .filter(col("g.concept_id_2") !== col("f.concept_id_2"))
      .selectExpr("g.concept_id_2 as concept_id_1", "f.concept_id_2 as concept_id_2")

  }

  val RELATIONSHIP = ("Maps to" :: Nil)

  val spark = SparkSession
    .builder()
    .appName("OMOP  calc")
    .enableHiveSupport()
    .getOrCreate()

  val empVertexDF = spark.read.table("edsomop.concept")
    .selectExpr("concept_id")
    .repartition(1000)

  val empEdgeDF = spark.read.table("edsomop.concept_relationship").as("s")
    .filter(col("relationship_id").isin(RELATIONSHIP))
    .filter(col("valid_end_date").<=(current_timestamp()))
    .selectExpr("concept_id_1", "concept_id_2")
    .repartition(1000)

  val hg = new HeritageGraph

  val empHirearchyExtDF = hg.calcTopLevelHierarcy(empVertexDF, empEdgeDF)
    .select(col("id").as("concept_id_1"), explode(col("path")).as("concept_id_2"))

  val candidate = generateCandidate(empHirearchyExtDF)
  val newCandidate = removeExistingCandidate(candidate, empEdgeDF)


  //.write.format("orc").mode(org.apache.spark.sql.SaveMode.Overwrite).saveAsTable("edsomop.orbis_patient_transco_mono_ids")

}
