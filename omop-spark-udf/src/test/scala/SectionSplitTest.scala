/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.spark.omop.section

import io.frama.parisni.spark.omop.graph.SparkSessionTestWrapper
import org.apache.spark.sql.QueryTest

class SectionSplitTest  extends QueryTest with SparkSessionTestWrapper  {


  test("test section") {
    import spark.implicits._
    val refSection = (
      ("AP", "ap", "bob", 2, "bob l'eponge")
        :: ("CD", "cd", "jim", 3, "jim le cacke")
        :: ("BG", "cd", "ji", 3, "jim le crack")
        :: Nil)
      .toDF("cd_section", "cd_doc", "label_section", "concept_id", "concept_name")

    val text =
      """
        |hello world
        |bob: hi
        |
        |jim: bye
        |end
        |""".stripMargin
    val note = (
      (1, text, "ap")
        :: (2, text, "cd")
        :: Nil)
      .toDF("note_id", "note_text", "note_class_source_value")


    val res = SectionSplit.explodeSection(spark, refSection, note, -1, 10)

    res.select("note_id", "offset_begin", "offset_end", "note_class_source_value", "note_class_source_concept_id", "note_title").show(false)
    //    assert(res.count() === 12)
  }

}

